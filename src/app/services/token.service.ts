import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class TokenService {
  token = "";
  deviceID = "";
  deviceType = "";

  constructor() {}

  setToken(token) {
    this.token = token;
  }

  setDeviceInfo(info: any) {
    this.deviceID = info.uuid;
    this.deviceType = info.operatingSystem;
  }

  setDeviceID(id: string) {
    this.deviceID = id;
  }
}
