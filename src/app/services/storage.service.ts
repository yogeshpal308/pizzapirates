import { Injectable } from "@angular/core";
import { Plugins } from "@capacitor/core";
const { Storage } = Plugins;
@Injectable({
  providedIn: "root",
})
export class StorageService {
  constructor() {}

  // Store the value
  async store(storageKey: string, value: any) {
    const encryptedValue = btoa(encodeURIComponent(JSON.stringify(value)));
    await localStorage.setItem(
      storageKey,
      encryptedValue,
    );
  }

  // Get the value
  async get(storageKey: string) {
    const ret = await localStorage.getItem(storageKey);
    if (ret) {
      return JSON.parse(decodeURIComponent(atob(ret)));
    } else {
      return false;
    }
  }

  // Remove the value
  async remove(storageKey: string) {
    await localStorage.removeItem(storageKey);
  }

  // JSON "get" example
  async getObject() {
    const ret = await localStorage.getItem("user");
    const user = JSON.parse(ret);
  }

  async setItem() {
    await localStorage.setItem(  "name"
      ,"Max"
    );
  }

  async keys() {
    const keys = await Storage.keys();
    console.log("Got keys: ", keys);
  }

  async clear() {
    await localStorage.clear();
  }
}
