import { Injectable } from "@angular/core";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { environment } from "../../environments/environment";
import { Constants } from "src/app/config/constants";
import { Subject } from "rxjs";
import { HttpService } from "./http.service";
import { Router } from "@angular/router";
import { StorageService } from "./storage.service";
import { AuthService } from "./auth.service";
import { LanguageService } from "./language.service";
import { SpinnerDialog } from "@ionic-native/spinner-dialog/ngx";
import { TranslateService } from "@ngx-translate/core";
@Injectable({
  providedIn: "root",
})
export class WebviewService {
  loading: Subject<boolean> = new Subject<boolean>();
  webviewClosed: Subject<boolean> = new Subject<boolean>();
  isLoggedIn = false;
  constructor(
    private iab: InAppBrowser,
    private router: Router,
    private httpService: HttpService,
    private storageService: StorageService,
    private authService: AuthService,
    private languageService: LanguageService,

    private spinnerDialog: SpinnerDialog,
    private _translate: TranslateService
  ) {}

  async goToWebview(session_id = "") {
    this.loading.next(true);

    const url = `${environment.baseUrl}${Constants.PROFILE}?app_token=${session_id}`;

    const options = `usewkwebview=yes,hidenavigationbuttons=yes,hideurlbar=yes,hardwareback=yes,location=no,zoom=no,mediaPlaybackRequiresUserAction=no,shouldPauseOnSuspend=no,closebuttoncaption=Close,disallowoverscroll=no,toolbar=no,enableViewportScale=no,allowInlineMediaPlayback=no,hidden=yes,fullscreen=no`;

    const browser = this.iab.create(url, "_blank", options);
    browser.on("message").subscribe((event) => {
      console.log("POSTMESSAGE", event);
      const user_data = JSON.parse(event.data.message.user_info);
      console.log("USERDATA", user_data);
      console.log("USERINFO", user_data.user_info);
      console.log("LOGGEDIN", user_data.logged_in);

      if (event.data.message) {
        const user_data = JSON.parse(event.data.message.user_info);
        const session_id = event.data.message.session_id;
        if (user_data) {
          this.storageService.remove(Constants.AUTH);
          if (user_data.logged_in) {
            this.isLoggedIn = true;
            //Storing the User data.
            const userData = { ...user_data };
            userData["session_id"] = session_id;
            this.authService.userData$.next(userData);
            this.storageService.store(Constants.AUTH, userData).then((res) => {
              this.router.navigate(["home"]);
            });
          } else {
            this.isLoggedIn = false;
            this.authService.userData$.next("");
          }
        }
      }
    });
    browser.on("loadstart").subscribe((event) => {
      browser.executeScript({
        code: `window.isIonicWebview = true;
        console.log('window.isIonicWebview LoadStart', window.isIonicWebview);`,
      });
    });
    browser.on("loadstop").subscribe((event) => {
      browser.executeScript({
        code: `window.isIonicWebview = true;
        console.log('window.isIonicWebview LoadStop', window.isIonicWebview);`,
      });
      this.loading.next(false);
      browser.show();
      //Close Webview
      if (event.url.match("/close-webview-ionic")) {
        browser.close();

        if (this.router.url === "/home/dashboard") {
          if (!this.isLoggedIn) window.location.reload();
        }
      }
    });
  }
}
