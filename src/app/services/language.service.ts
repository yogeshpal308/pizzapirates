import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Constants } from "../config/constants";
import { StorageService } from "./storage.service";

@Injectable({
  providedIn: "root",
})
export class LanguageService {
  language: string;
  languageCode$ = new BehaviorSubject<any>("");
  constructor(private storageService: StorageService) {}

  getStoredLanguage() {
    this.storageService.get(Constants.LANGUAGECODE).then((res) => {
      this.language = res;
      this.languageCode$.next(res);
    });
  }

  setStoredLanguage(language: string) {
    this.storageService.store(Constants.LANGUAGECODE, language).then((res) => {
      this.language = language;
      this.languageCode$.next(language);
    });
  }

  getUserLanguage() {
    return this.language;
  }

  setUserLanguage(language: string) {
    this.language = language;
  }
}
