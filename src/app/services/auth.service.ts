import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { Constants } from "../config/constants";
import { HttpService } from "./http.service";
import { StorageService } from "./storage.service";
import { ToastService } from "./toast.service";
import { TranslateService } from "@ngx-translate/core";
import { LanguageService } from "./language.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  userData$ = new BehaviorSubject<any>([]);

  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private router: Router,
    private toastService: ToastService,
    private _translate: TranslateService,
    private languageService: LanguageService
  ) {}

  getUserData() {
    this.storageService.get(Constants.AUTH).then((res) => {
      this.userData$.next(res);
    });
  }

  login(postData: any): Observable<any> {
    return this.httpService.post("user", postData);
  }

  signup(postData: any): Observable<any> {
    return this.httpService.post("user", postData);
  }

  forgotPassword(postData: any): Observable<any> {
    return this.httpService.post("user", postData);
  }

  logout(postData) {
    this.httpService.post("user", postData).subscribe(
      (res: any) => {
        console.log(res);
        if (res.success == "true") {
          this.storageService.remove(Constants.AUTH);
          this.userData$.next("");
          window.location.reload();
        } else {
          this.toastService.presentToast(
            this._translate.instant("Error_Occured")
          );
        }
      },
      (err: any) => {
        console.log("Error", err);
        this.toastService.presentToast(
          this._translate.instant("Error_Occured")
        );
      }
    );
  }
}
