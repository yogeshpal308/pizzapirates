import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpService } from "./http.service";
import { Constants } from "../config/constants";
import { StorageService } from "./storage.service";
@Injectable({
  providedIn: "root",
})
export class DashboardService {
  printedOrders = [];

  constructor(
    private httpService: HttpService,
    private storageService: StorageService
  ) {}
}
