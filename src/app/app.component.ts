import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { PushNotification, PushNotificationActionPerformed, PushNotificationToken,ActionPerformed,PushNotificationSchema, PushNotifications, Token } from "@capacitor/push-notifications";
// import { Plugins, PushNotification, PushNotificationActionPerformed, PushNotificationToken } from "@capacitor/core";
import { TokenService } from "./services/token.service";
import { Constants } from "./config/constants";
import { ToastService } from "./services/toast.service";
import { HttpService } from "./services/http.service";
import { Location } from "@angular/common";
import { Plugins } from '@capacitor/core';
import { App } from '@capacitor/app';

import { Device } from '@capacitor/device';

// const { PushNotifications } = Plugins;

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {
  navLinksArray = []; // store route links as the user navigates the app
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private tokenService: TokenService,
    private toastService: ToastService,
    private httpService: HttpService,
    private _location: Location
  ) {
    this.initializeApp();

    this.deviceInfo();

    this.router.events.subscribe((event) => {
      const url = this.router.url; //current url
      if (event instanceof NavigationEnd) {
        const isCurrentUrlSaved = this.navLinksArray.find(
          (item) => item === url
        );
        if (!isCurrentUrlSaved) this.navLinksArray.push(url);
      }
    });

    this.hardwareBackButton();
  }
  

  async deviceInfo() {
    const info = await Device.getInfo();
    console.log("DEVICE INFO " + JSON.stringify(info));
    //alert("DEVICE INFO " + JSON.stringify(info));
    this.tokenService.setDeviceInfo(info);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  hardwareBackButton() {
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (this.navLinksArray.length > 1) {
        if (
          this.router.url == "/signup" ||
          this.router.url == "/home/dashboard" ||
          this.router.url == "/home/profile"
        ) {
          App.exitApp();
        } else {
          this.navLinksArray.pop();
          const index = this.navLinksArray.length - 1;
          const url = this.navLinksArray[index];
          this.router.navigate([url]);
        }
      } else {
        App.exitApp();
      }
    });
  }

  ngOnInit(): void {
    
    PushNotifications.requestPermissions().then((PermissionStatus) => {
      if (PermissionStatus) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
        this.toastService.presentToast(
          "Please grant permission to receive push notification."
        );
      }
    });

    PushNotifications.addListener(
      "registration",
      (token: Token) => {
        console.log(`Push registration success, token: ${token.value}`);
        //alert("Push registration success, token: " + token.value);
        this.tokenService.setToken(token.value);
        const data = {
          profile_id: Constants.PROFILEID,
          api_key: Constants.APIKEY,
          request_operation: "register_device", // "register device"
          token: token.value,
          device_id: this.tokenService.deviceID,
          device_type: this.tokenService.deviceType,
          type: this.tokenService.deviceType,
          app_type: "ionic",
          //session_id: this.authUser?.session_id,
        };
        console.log("This is api data ", data);
        
        this.httpService
          .post("push-register", data)
          .subscribe(
            (res: any) => {
              //alert("PUSH REGISTER API HIT SUCCESS." + JSON.stringify(res));
              if (res.success == "true") {
                console.log("Device registered for pusher service.");
              }
            },
            (err: any) => {
              //alert("PUSH REGISTER API HIT ERROR." + JSON.stringify(err));
              console.log("Error", err);
              throw new Error(err);
            }
          );
      }
    );

    PushNotifications.addListener("registrationError", (error: any) => {
      console.log("Error on registration: " + JSON.stringify(error));
      //alert("Error on registration: " + JSON.stringify(error));
      throw new Error(error);
    });

    PushNotifications.createChannel({
      description: "General Notifications",
      id: "fcm_default_channel",
      importance: 5,
      lights: true,
      name: "My notification channel",
      sound: "",
      vibration: true,
      visibility: 1,
    })
      .then(() => {
        console.log("push channel created: ");
      })
      .catch((error) => {
        console.error("push channel error: ", error);
      });

    PushNotifications.addListener(
      "pushNotificationReceived",
       async (notification: PushNotificationSchema) => {
        console.log("NotificationReceived", notification);
        //alert("NotificationReceived" + JSON.stringify(notification));
        //Local Notification
        // await LocalNotifications.schedule({
        //   notifications: [
        //     {
        //       title: "Title",
        //       body: "Body",
        //       id: 1,
        //       sound: null,
        //       attachments: null,
        //       actionTypeId: "",
        //       extra: null
        //     }
        //   ]
        // });

        let msg = notification.body;
        //this.alertService.presentAlertConfirm("New Order Received", msg);

        if (this.platform.is("capacitor")) {
          this.platform.resume.subscribe(() => {
            if (this._location.isCurrentPathEqualTo("/home/dashboard")) {
              window.location.reload();
            }
          });
        }
      }
    );

    PushNotifications.addListener(
      "pushNotificationActionPerformed",
      (notification: PushNotificationActionPerformed) => {
        console.log("NotificationActionPerformed", notification.notification);
       
        if (this.platform.is("capacitor")) {
          this.platform.resume.subscribe(() => {
            if (this._location.isCurrentPathEqualTo("/home/dashboard")) {
              window.location.reload();
            }
          });
        }
      }
    );
    
  }
    
}

