export class Constants {
  public static readonly AUTH = "userData";
  public static readonly LANGUAGECODE = "languageCode";
  public static readonly APIKEY = "";
  public static readonly PROFILEID = 907597;
  public static readonly PROFILE = "elpatronsteakhouseny";
  public static readonly LANGUAGE = "en";
  public static readonly PRIVACYLINK =
    "https://app1.restolabs.com/privacy-policy/10727/1";
  public static readonly TERMSLINK =
    "https://app1.restolabs.com/terms-of-use/10727/1";
}

