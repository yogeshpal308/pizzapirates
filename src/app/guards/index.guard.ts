import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Constants } from "../config/constants";
import { StorageService } from "../services/storage.service";
import { HttpService } from "../services/http.service";
import { AuthService } from "../services/auth.service";
@Injectable({
  providedIn: "root",
})
export class IndexGuard implements CanActivate {
  constructor(
    public storageService: StorageService,
    public router: Router,
    public httpService: HttpService,
    public authService: AuthService
  ) {}
  canActivate(): Promise<boolean> {
    return new Promise((resolve) => {
      this.storageService
        .get(Constants.AUTH)
        .then((res) => {
          console.log("INDEX GUARD CALLED");
          if (res) {
            const data = {
              profile_id: Constants.PROFILEID,
              api_key: Constants.APIKEY,
              app_version: "",
              session_id: res.session_id,
              request_operation: "user_status",
              language: Constants.LANGUAGE,
            };
            this.httpService.post("user", data).subscribe(
              (res: any) => {
                console.log("CHECKING USER STATUS", res);
                if (res.response.logged_in) {
                  resolve(false);
                  this.router.navigate(["home"]);
                } else {
                  this.storageService.remove(Constants.AUTH);
                  this.authService.userData$.next("");
                  resolve(true);
                }
              },
              (err: any) => {
                console.error(err);
                resolve(true);
              }
            );
          } else resolve(true);
        })
        .catch((err) => {
          resolve(true);
        });
    });
  }
}
