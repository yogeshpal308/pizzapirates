import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Constants } from "../config/constants";
import { StorageService } from "../services/storage.service";

@Injectable({
  providedIn: "root",
})
export class HomeGuard implements CanActivate {
  constructor(public storageService: StorageService, public router: Router) {}
  canActivate(): Promise<boolean> {
    return new Promise((resolve) => {
      this.storageService
        .get(Constants.AUTH)
        .then((res) => {
          console.log("HOME GUARD CALLED");
          if (res) {
            resolve(true);
          } else {
            this.router.navigate([""]);
            resolve(false);
          }
        })
        .catch((err) => {
          resolve(false);
        });
    });
  }
}
