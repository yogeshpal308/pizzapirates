import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { IndexGuard } from "../guards/index.guard";
import { IndexPage } from "./index.page";
import { LanguageCodeResolver } from "../resolvers/language-code.resolver";

const routes: Routes = [
  {
    path: "",
    component: IndexPage,
    canActivate: [IndexGuard],
    resolve: {
      languageCode: LanguageCodeResolver,
    },
    children: [
      {
        path: "login",
        loadChildren: () =>
          import("../pages/login/login.module").then((m) => m.LoginPageModule),
      },
      {
        path: "signup",
        loadChildren: () =>
          import("../pages/signup/signup.module").then(
            (m) => m.SignupPageModule
          ),
      },
      {
        path: "forgot-password",
        loadChildren: () =>
          import("../pages/forgot-password/forgot-password.module").then(
            (m) => m.ForgotPasswordPageModule
          ),
      },
      {
        path: "",
        redirectTo: "/signup",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndexRouter {}
