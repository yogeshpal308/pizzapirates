import { Component, OnInit } from "@angular/core";
import { IOSPushOptions } from "@ionic-native/push";
import { PermissionStatus } from "@capacitor/push-notifications/dist/esm/definitions";
import { PushNotification, PushNotificationActionPerformed, PushNotificationToken,ActionPerformed,PushNotificationSchema, PushNotifications, Token } from "@capacitor/push-notifications";
// import { PushNotificationToken,PushNotification, Plugins, PushNotificationActionPerformed } from "@capacitor/core";
import { Platform } from "@ionic/angular";
import { Constants } from "../config/constants";
import { AlertService } from "../services/alert.service";
import { AuthService } from "../services/auth.service";
import { DashboardService } from "../services/dashboard.service";
import { HttpService } from "../services/http.service";
import { ToastService } from "../services/toast.service";
import { TokenService } from "../services/token.service";
// const { PushNotifications } = Plugins;
// const { LocalNotifications } = Plugins;
import { Location } from "@angular/common";
import { Plugins } from "protractor/built/plugins";


@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  authUser: any;
  constructor(
    private httpService: HttpService,
    private dashboardService: DashboardService,
    private authService: AuthService,
    private toastService: ToastService,
    private tokenService: TokenService,
    private alertService: AlertService,
    private platform: Platform,
    private _location: Location
  ) {}

  ngOnInit() {
    
    this.authService.userData$.subscribe((res: any) => {
      this.authUser = res;
    });

    // PushNotifications.requestPermission().then((result) => {
    //   if (result.granted) {
    //     // Register with Apple / Google to receive push via APNS/FCM
    //     PushNotifications.register();
    //   } else {
    //     // Show some error
    //     this.toastService.presentToast(
    //       "Please grant permission to receive push notification."
    //     );
    //   }
    // });

    // PushNotifications.addListener(
    //   "registration",
    //   (token: PushNotificationToken) => {
    //     console.log(`Push registration success, token: ${token.value}`);
    //     //alert("Push registration success, token: " + token.value);
    //     this.tokenService.setToken(token.value);
    //     const data = {
    //       profile_id: Constants.PROFILEID,
    //       api_key: Constants.APIKEY,
    //       request_operation: "register_device", // "register device"
    //       token: token.value,
    //       device_id: this.tokenService.deviceID,
    //       device_type: this.tokenService.deviceType,
    //       type: this.tokenService.deviceType,
    //       app_type: "ionic",
    //       session_id: this.authUser?.session_id,
    //     };
    //     console.log("This is api data ", data);
        
    //     this.httpService
    //       .post("push-register", data)
    //       .subscribe(
    //         (res: any) => {
    //           //alert("PUSH REGISTER API HIT SUCCESS." + JSON.stringify(res));
    //           if (res.success == "true") {
    //             console.log("Device registered for pusher service.");
    //           }
    //         },
    //         (err: any) => {
    //           //alert("PUSH REGISTER API HIT ERROR." + JSON.stringify(err));
    //           console.log("Error", err);
    //           throw new Error(err);
    //         }
    //       );
    //   }
    // );

    // PushNotifications.addListener("registrationError", (error: any) => {
    //   console.log("Error on registration: " + JSON.stringify(error));
    //   //alert("Error on registration: " + JSON.stringify(error));
    //   throw new Error(error);
    // });

    // PushNotifications.createChannel({
    //   description: "General Notifications",
    //   id: "fcm_default_channel",
    //   importance: 5,
    //   lights: true,
    //   name: "My notification channel",
    //   sound: "",
    //   vibration: true,
    //   visibility: 1,
    // })
    //   .then(() => {
    //     console.log("push channel created: ");
    //   })
    //   .catch((error) => {
    //     console.error("push channel error: ", error);
    //   });

    // PushNotifications.addListener(
    //   "pushNotificationReceived",
    //    async (notification: PushNotification) => {
    //     console.log("NotificationReceived", notification);
    //     //alert("NotificationReceived" + JSON.stringify(notification));
    //     //Local Notification
    //     // await LocalNotifications.schedule({
    //     //   notifications: [
    //     //     {
    //     //       title: "Title",
    //     //       body: "Body",
    //     //       id: 1,
    //     //       sound: null,
    //     //       attachments: null,
    //     //       actionTypeId: "",
    //     //       extra: null
    //     //     }
    //     //   ]
    //     // });

    //     let msg = notification.body;
    //     //this.alertService.presentAlertConfirm("New Order Received", msg);

    //     if (this.platform.is("capacitor")) {
    //       this.platform.resume.subscribe(() => {
    //         if (this._location.isCurrentPathEqualTo("/home/dashboard")) {
    //           window.location.reload();
    //         }
    //       });
    //     }
    //   }
    // );

    // PushNotifications.addListener(
    //   "pushNotificationActionPerformed",
    //   (notification: PushNotificationActionPerformed) => {
    //     console.log("NotificationActionPerformed", notification.notification);
       
    //     if (this.platform.is("capacitor")) {
    //       this.platform.resume.subscribe(() => {
    //         if (this._location.isCurrentPathEqualTo("/home/dashboard")) {
    //           window.location.reload();
    //         }
    //       });
    //     }
    //   }
    // );
    
  }
}
