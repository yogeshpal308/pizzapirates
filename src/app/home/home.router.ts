import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeGuard } from "../guards/home.guard";
import { UserDataResolver } from "../resolvers/user-data.resolver";
import { LanguageCodeResolver } from "../resolvers/language-code.resolver";
import { HomePage } from "./home.page";

const routes: Routes = [
  {
    path: "home",
    component: HomePage,
    canActivate: [HomeGuard],
    resolve: {
      userData: UserDataResolver,
      languageCode: LanguageCodeResolver,
    },
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("../pages/dashboard/dashboard.module").then(
            (m) => m.DashboardPageModule
          ),
      },
      {
        path: "profile",
        loadChildren: () =>
          import("../pages/profile/profile.module").then(
            (m) => m.ProfilePageModule
          ),
      },

      {
        path: "",
        redirectTo: "/home/dashboard",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRouter {}
