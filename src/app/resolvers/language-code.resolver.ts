import { Injectable } from "@angular/core";
import { LanguageService } from "../services/language.service";

@Injectable({
  providedIn: "root",
})
export class LanguageCodeResolver {
  constructor(private languageService: LanguageService) {}

  resolve() {
    return this.languageService.getStoredLanguage();
  }
}
