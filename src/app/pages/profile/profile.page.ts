import { Component, OnInit } from "@angular/core";
import { AuthService } from "./../../services/auth.service";
import { TokenService } from "src/app/services/token.service";
import { Constants } from "../../config/constants";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  authUser: any;
  constructor(
    private authService: AuthService,
    private tokenService: TokenService
  ) {}

  ngOnInit() {
    this.authService.userData$.subscribe((res: any) => {
      this.authUser = res;
      console.log(this.authUser);
    });
  }

  logoutAction() {
    this.authService.logout({
      profile_id: Constants.PROFILEID,
      user_id: this.authUser?.user_id,
      api_key: Constants.APIKEY,
      app_version: "",
      request_operation: "user_logout",
      session_id: this.authUser?.session_id,
      language: Constants.LANGUAGE,
    });
  }
}
