import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Constants } from "../../config/constants";
import { AuthService } from "./../../services/auth.service";
import { StorageService } from "./../../services/storage.service";
import { ToastService } from "./../../services/toast.service";
import { TranslateService } from "@ngx-translate/core";
import { WebviewService } from "src/app/services/webview.service";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.page.html",
  styleUrls: ["./forgot-password.page.scss"],
})
export class ForgotPasswordPage implements OnInit {
  postData = {
    profile_id: Constants.PROFILEID,
    api_key: Constants.APIKEY,
    usermail: "",
    app_version: "",
    request_operation: "user_password_reset_mail",
    language: Constants.LANGUAGE,
  };
  loading = false;
  constructor(
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService,
    private webviewService: WebviewService,
    private toastService: ToastService,
    private _translate: TranslateService
  ) {}

  ngOnInit() {}

  goToWebview() {
    this.webviewService.goToWebview();
  }

  goToLogIn() {
    this.router.navigate(["/login"]);
  }

  goToPrivacyLink() {
    window.open(Constants.PRIVACYLINK);
  }

  goToTermsLink() {
    window.open(Constants.TERMSLINK);
  }

  validateInputs() {
    console.log(this.postData);
    let usermail = this.postData.usermail.trim();
    return this.postData.usermail && usermail.length > 0;
  }

  submit() {
    if (this.validateInputs()) {
      this.authService.login(this.postData).subscribe(
        (res: any) => {
          console.log(res);
          if (res.success == "true") {
            this.toastService.presentToast(res.response.message);
            this.toastService.presentToast(
              this._translate.instant("Response_Message", {
                value: res.response.message,
              })
            );
          } else {
            this.toastService.presentToast(
              this._translate.instant("Response_Message", {
                value: res.error.message,
              })
            );
          }
        },
        (err: any) => {
          console.log(err);

          this.toastService.presentToast(
            this._translate.instant("Error_Message")
          );
        }
      );
    } else {
      this.toastService.presentToast(this._translate.instant("Invalid_Email"));
    }
  }
}
