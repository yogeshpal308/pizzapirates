import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Constants } from "../../config/constants";
import { AuthService } from "./../../services/auth.service";
import { StorageService } from "./../../services/storage.service";
import { ToastService } from "./../../services/toast.service";
import { TranslateService } from "@ngx-translate/core";
import { LanguageService } from "src/app/services/language.service";
import { WebviewService } from "src/app/services/webview.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  postData = {
    profile_id: Constants.PROFILEID,
    api_key: Constants.APIKEY,
    usermail: "",
    password: "",
    app_version: "",
    request_operation: "user_login",
    language: Constants.LANGUAGE,
  };
  loading = false;
  userLanguage: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService,
    private toastService: ToastService,
    private _translate: TranslateService,
    private languageService: LanguageService,
    private webviewService: WebviewService
  ) {}

  ngOnInit() {
    this.userLanguage = this.languageService.getUserLanguage();
  }
  goToWebview() {
    this.webviewService.goToWebview();
  }

  goToSignUp() {
    this.router.navigate(["/signup"]);
  }

  goToForgotPassword() {
    this.router.navigate(["/forgot-password"]);
  }

  goToPrivacyLink() {
    window.open(Constants.PRIVACYLINK);
  }

  goToTermsLink() {
    window.open(Constants.TERMSLINK);
  }

  validateInputs() {
    console.log(this.postData);
    let usermail = this.postData.usermail.trim();
    let password = this.postData.password.trim();
    return (
      this.postData.usermail &&
      this.postData.password &&
      usermail.length > 0 &&
      password.length > 0
    );
  }

  loginAction() {
    if (this.validateInputs()) {
      this.authService.login(this.postData).subscribe(
        (res: any) => {
          console.log(res);
          if (res.success == "true") {
            //Storing the User data.
            const userData = { ...res.response };
            userData["session_id"] = res.session_id;
            userData["session_name"] = res.session_name;
            this.storageService.store(Constants.AUTH, userData).then((res) => {
              this.router.navigate(["home"]);
            });
          } else {
            this.toastService.presentToast(
              this._translate.instant("Response_Message", {
                value: res.error.message,
              })
            );
          }
        },
        (err: any) => {
          console.log(err);
          this.toastService.presentToast(
            this._translate.instant("Error_Message")
          );
        }
      );
    } else {
      this.toastService.presentToast(
        this._translate.instant("Invalid_Credentials")
      );
    }
  }
}
