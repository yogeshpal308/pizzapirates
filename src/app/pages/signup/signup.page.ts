import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Constants } from "../../config/constants";
import { AuthService } from "./../../services/auth.service";
import { ToastService } from "src/app/services/toast.service";
import { StorageService } from "src/app/services/storage.service";
import { WebviewService } from "src/app/services/webview.service";
import { SpinnerDialog } from "@ionic-native/spinner-dialog/ngx";
import { TranslateService } from "@ngx-translate/core";
import { LanguageService } from "src/app/services/language.service";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.page.html",
  styleUrls: ["./signup.page.scss"],
})
export class SignupPage implements OnInit {
  postData = {
    profile_id: Constants.PROFILEID,
    api_key: Constants.APIKEY,
    first_name: "",
    last_name: "",
    usermail: "",
    userphone: "",
    password: "",
    app_version: "",
    request_operation: "user_register",
    language: Constants.LANGUAGE,
    gdpr_consent: 1,
  };

  isChecked = true;

  loading = false;
  languages = [
    ["en", "English"],
    ["es", "Spanish"],
    ["fi", "Finnish"],
    ["fr", "French"],
  ];

  language: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastService: ToastService,
    private storageService: StorageService,
    private webviewService: WebviewService,
    private spinnerDialog: SpinnerDialog,
    private _translate: TranslateService,
    private languageService: LanguageService
  ) {
    this.getDeviceLanguage();
  }

  ngOnInit() {
    this.webviewService.loading.subscribe((value) => {
      this.loading = value;
      if (value) {
        this.spinnerDialog.show(
          null,
          this._translate.instant("Loading_Message"),
          true
        );
      } else {
        this.spinnerDialog.hide();
      }
    });

    this.languageService.languageCode$.subscribe((res) => {
      if (res) {
        this.language = res;
        this._initTranslate(res);
      }
    });
  }

  goToLogIn() {
    this.router.navigate(["/login"]);
  }

  goToPrivacyLink() {
    window.open(Constants.PRIVACYLINK);
  }

  goToTermsLink() {
    window.open(Constants.TERMSLINK);
  }

  validateInputs() {
    console.log(this.postData);
    let first_name = this.postData.first_name.trim();
    let last_name = this.postData.last_name.trim();
    let usermail = this.postData.usermail.trim();
    let userphone = this.postData.userphone.trim();
    let password = this.postData.password.trim();
    return (
      first_name &&
      last_name &&
      usermail &&
      userphone &&
      password &&
      first_name.length > 0 &&
      last_name.length > 0 &&
      usermail.length > 0 &&
      userphone.length > 0 &&
      password.length > 0
    );
  }

  signup() {
    if (this.validateInputs()) {
      this.authService.signup(this.postData).subscribe(
        (res: any) => {
          console.log(res);
          if (res.success == "true") {
            this.toastService.presentToast(
              this._translate.instant("Signup_Success")
            );
            this.router.navigate(["login"]);
          } else {
            this.toastService.presentToast(
              this._translate.instant("Response_Message", {
                value: res.error.message,
              })
            );
          }
        },
        (err: any) => {
          console.error(err);
          this.toastService.presentToast(
            this._translate.instant("Error_Message")
          );
        }
      );
    } else {
      this.toastService.presentToast(this._translate.instant("Required"));
    }
  }

  goToWebview() {
    this.webviewService.goToWebview();
  }

  public changeLanguage(): void {
    this.languageService.setStoredLanguage(this.language);
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang("en");
    if (language) {
      this.language = language;
    } else {
      // Set your language here
      this.language = "en";
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === "object") {
      if (navigator.language.split("-").length > 0) {
        const detectedLanguage = navigator.language.split("-")[0];
        this._initTranslate(detectedLanguage);
      }
    }
  }
}
