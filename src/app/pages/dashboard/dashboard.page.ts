import { Component, ViewChild, OnInit } from "@angular/core";
import { DashboardService } from "./../../services/dashboard.service";
import { ToastService } from "./../../services/toast.service";
import { AuthService } from "src/app/services/auth.service";
import { Router } from "@angular/router";
import { WebviewService } from "src/app/services/webview.service";
import { SpinnerDialog } from "@ionic-native/spinner-dialog/ngx";
import { LanguageService } from "src/app/services/language.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"],
})
export class DashboardPage implements OnInit {
  loading = false;
  authUser: any;
  language: string;
  constructor(
    private dashboardService: DashboardService,
    private toastService: ToastService,
    private router: Router,
    private authService: AuthService,
    private webviewService: WebviewService,
    private spinnerDialog: SpinnerDialog,
    private languageService: LanguageService,
    private _translate: TranslateService
  ) {}

  ngOnInit() {
    this.authService.userData$.subscribe((res: any) => {
      console.log(res);
      this.authUser = res;
    });

    this.languageService.languageCode$.subscribe((res) => {
      if (res) {
        this.language = res;
        this._initTranslate(res);
      }
    });

    this.webviewService.loading.subscribe((value) => {
      console.log("LOADING STATE", value);
      this.loading = value;
      if (value) {
        this.spinnerDialog.show(
          null,
          this._translate.instant("Loading_Message"),
          true
        );
      } else {
        this.spinnerDialog.hide();
      }
    });
  }

  goToInfo() {
    window.open("https://www.kochloeffel.jp/aboutus");
  }

  goToDSTY() {
    window.open("https://www.dstymensa.com");
  }

  goToWebview() {
    this.webviewService.goToWebview(this.authUser.session_id);
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang("en");
    if (language) {
      this.language = language;
    } else {
      // Set your language here
      this.language = "en";
    }
    this._translateLanguage();
  }
}
