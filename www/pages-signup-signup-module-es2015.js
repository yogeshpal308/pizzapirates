(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-signup-signup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/signup/signup.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/signup/signup.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen class=\"ion-no-padding\">\r\n  <ion-header translucent>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button\r\n          defaultHref=\"/\"\r\n          text=\"\"\r\n          icon=\"chevron-back-outline\"\r\n        ></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n        <ion-button\r\n          class=\"ion-float-right\"\r\n          size=\"full\"\r\n          fill=\"solid\"\r\n          size=\"small\"\r\n          (click)=\"goToWebview()\"\r\n          translate\r\n        >\r\n          Skip_Sign_Up</ion-button\r\n        >\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <ion-img\r\n    class=\"logo__image\"\r\n    src=\"../../../assets/images/register-bg.png\"\r\n    [attr.alt]=\"'Restaurant_Logo' | translate\"\r\n  >\r\n  </ion-img>\r\n  <!-- <ion-item>\r\n    <ion-select\r\n      [(ngModel)]=\"language\"\r\n      [value]=\"language\"\r\n      (ionChange)=\"changeLanguage()\"\r\n    >\r\n      <ion-select-option\r\n        *ngFor=\"let language of languages\"\r\n        value=\"{{language[0]}}\"\r\n        >{{language[1]}}</ion-select-option\r\n      >\r\n    </ion-select>\r\n  </ion-item> -->\r\n\r\n  <ion-grid class=\"ion-padding\">\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"person-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            type=\"text\"\r\n            class=\"form-control\"\r\n            autocomplete=\"off\"\r\n            placeholder=\"{{'First_Name' | translate}}*\"\r\n            required\r\n            [(ngModel)]=\"postData.first_name\"\r\n            #first_name=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"first_name.invalid && (first_name.dirty || first_name.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"first_name.errors.required\" class=\"invalid\">\r\n            First Name is required.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"person-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"text\"\r\n            required\r\n            placeholder=\"{{'Last_Name' | translate}}*\"\r\n            [(ngModel)]=\"postData.last_name\"\r\n            #last_name=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"last_name.invalid && (last_name.dirty || last_name.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"last_name.errors.required\" class=\"invalid\">\r\n            Last Name is required.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"mail-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"email\"\r\n            required\r\n            pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$\"\r\n            placeholder=\"{{'Email' | translate}}*\"\r\n            [(ngModel)]=\"postData.usermail\"\r\n            #usermail=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"usermail.invalid && (usermail.dirty || usermail.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"usermail.errors.required\" class=\"invalid\">\r\n            Email is required.\r\n          </p>\r\n          <p *ngIf=\"usermail.errors.pattern\" class=\"invalid\">\r\n            Please enter valid email address.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"call-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"tel\"\r\n            required\r\n            pattern=\"[0-9]{3}[0-9]{3}[0-9]{4}\"\r\n            placeholder=\"{{'Mobile_Number' | translate}}*\"\r\n            [(ngModel)]=\"postData.userphone\"\r\n            #userphone=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"userphone.invalid && (userphone.dirty || userphone.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"userphone.errors.required\" class=\"invalid\">\r\n            Mobile Number is required.\r\n          </p>\r\n          <p *ngIf=\"userphone.errors.pattern\" class=\"invalid\">\r\n            Mobile Number must be 10 digits long.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"lock-closed-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"password\"\r\n            required\r\n            minlength=\"6\"\r\n            placeholder=\"{{'Password' | translate}}*\"\r\n            [(ngModel)]=\"postData.password\"\r\n            #password=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"password.invalid && (password.dirty || password.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"password.errors.required\" class=\"invalid\">\r\n            Password is required.\r\n          </p>\r\n          <p *ngIf=\"password.errors.minlength\" class=\"invalid\">\r\n            Password must be at least 6 characters long.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"terms_checkbox\">\r\n      <ion-col>\r\n        <ion-checkbox [(ngModel)]=\"isChecked\" checked=\"true\"></ion-checkbox>\r\n        <span class=\"privacy__policy\"\r\n          ><span style=\"padding-right: 5px\" translate>Agree_to</span>\r\n          <span class=\"ext__link\" (click)=\"goToPrivacyLink()\" translate\r\n            >Privacy_Policy</span\r\n          >\r\n          <span style=\"padding: 0 5px\" translate>And</span>\r\n          <span class=\"ext__link\" (click)=\"goToTermsLink()\" translate\r\n            >Terms_Conditions</span\r\n          >.</span\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"sign-up-button-row\">\r\n      <ion-col>\r\n        <ion-button\r\n          class=\"sign-up-button\"\r\n          size=\"full\"\r\n          fill=\"solid\"\r\n          size=\"large\"\r\n          (click)=\"signup()\"\r\n          translate\r\n          >Sign_Up</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n    <!-- <ion-row>\r\n      <ion-col>\r\n        <ion-button size=\"block\" (click)=\"goToWebview()\" translate\r\n          >Skip_Sign_Up</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row> -->\r\n    <ion-row class=\"already_have_account\">\r\n      <ion-col>\r\n        <span class=\"have_account_link\" (click)=\"goToLogIn()\" translate\r\n          >Have_Account</span\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <ion-row>\r\n    <div class=\"terms_wrapper\">\r\n      <span translate>By_continue</span>\r\n      <span>\r\n        <a (click)=\"goToTermsLink()\" translate>Terms_Conditions</a>\r\n        <a (click)=\"goToPrivacyLink()\" translate>Privacy_Policy</a>\r\n      </span>\r\n    </div>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/signup/signup.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/signup/signup.module.ts ***!
  \***********************************************/
/*! exports provided: HttpLoaderFactory, SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup.page */ "./src/app/pages/signup/signup.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");










function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__["TranslateHttpLoader"](http, "./assets/i18n/", ".json");
}
const routes = [
    {
        path: "",
        component: _signup_page__WEBPACK_IMPORTED_MODULE_5__["SignupPage"],
    },
];
let SignupPageModule = class SignupPageModule {
};
SignupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild({
                loader: {
                    provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
                    useFactory: HttpLoaderFactory,
                    deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]],
                },
            }),
        ],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_5__["SignupPage"]],
    })
], SignupPageModule);



/***/ }),

/***/ "./src/app/pages/signup/signup.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/signup/signup.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-icon {\n  margin: 5px !important;\n}\n\nion-header ion-toolbar:first-of-type {\n  --background: var(--ion-background, #000000);\n  color: var(--ion-font, #ffffff);\n}\n\nion-content {\n  --background: var(--ion-background, #000000);\n}\n\nion-item {\n  border-radius: 12px;\n  border: none;\n}\n\nion-grid {\n  color: var(--ion-font, #ffffff);\n  margin: 20px;\n  background-color: #8CB58D;\n  border-radius: 20px;\n}\n\nion-checkbox {\n  margin-right: 5px;\n}\n\n.privacy__policy {\n  vertical-align: top;\n}\n\n.ext__link {\n  vertical-align: top;\n  color: #3880ff;\n}\n\n.skip {\n  color: #ffffff;\n  background-color: #3880ff;\n  --border-radius: 4px;\n  --padding-top: 0;\n  --padding-bottom: 0;\n  --padding-start: 1.1em;\n  --padding-end: 1.1em;\n  --transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1),\n    background-color 15ms linear, color 15ms linear;\n}\n\nion-input {\n  margin-left: 15px !important;\n}\n\n.terms_wrapper {\n  display: flex;\n  flex-flow: column;\n  justify-content: center;\n  width: 100%;\n  align-items: center;\n  font-size: 11px;\n  margin: 1em 0;\n}\n\n.terms_wrapper a {\n  color: #000;\n  margin-right: 5px;\n  text-decoration: underline;\n}\n\n.terms_checkbox {\n  display: none;\n}\n\n.already_have_account {\n  text-align: center;\n  margin-top: 25px;\n}\n\n.sign-up-button-row {\n  text-align: center;\n  margin: 1em 0;\n}\n\n.sign-up-button {\n  --border-radius: 0;\n  --padding-start: 15vw;\n  --padding-end: 15vw;\n}\n\n.ion-toolbar-custom {\n  display: flex;\n  padding-right: 10px;\n}\n\n.have_account_link {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2lnbnVwL0M6XFxVc2Vyc1xceW9nZXNcXE9uZURyaXZlXFxEZXNrdG9wXFxwaXp6cGlhcmV0c2FmdGVybm9jb2xvclxccGl6emFwaXJhdGVzLTIwMjMwNTE4VDE2MjAyOVotMDAxXFxwaXp6YXBpcmF0ZXMvc3JjXFxhcHBcXHBhZ2VzXFxzaWdudXBcXHNpZ251cC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3NpZ251cC9zaWdudXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usc0JBQUE7QUNDRjs7QURFQTtFQUNFLDRDQUFBO0VBQ0EsK0JBQUE7QUNDRjs7QURHQTtFQUNFLDRDQUFBO0FDQUY7O0FER0E7RUFDRSxtQkFBQTtFQUNBLFlBQUE7QUNBRjs7QURLQTtFQUNFLCtCQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUNGRjs7QURLQTtFQUNFLGlCQUFBO0FDRkY7O0FES0E7RUFDRSxtQkFBQTtBQ0ZGOztBREtBO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0FDRkY7O0FES0E7RUFDRSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0VBQ0E7bURBQUE7QUNERjs7QURLQTtFQUNFLDRCQUFBO0FDRkY7O0FES0E7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0FDRkY7O0FER0U7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtBQ0RKOztBREtBO0VBQ0UsYUFBQTtBQ0ZGOztBREtBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQ0ZGOztBREtBO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0FDRkY7O0FES0E7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNGRjs7QURLQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQ0ZGOztBREtBO0VBQ0UsZUFBQTtBQ0ZGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2lnbnVwL3NpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taWNvbiB7XHJcbiAgbWFyZ2luOiA1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBlIHtcclxuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLCAjMDAwMDAwKTtcclxuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xyXG4gIC8vIGJveC1zaGFkb3c6IDBweCAycHggMHB4IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24tY29udGVudCB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG4vLyBpb24tYnV0dG9uIHtcclxuLy8gICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xyXG4vLyB9XHJcbmlvbi1ncmlkIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xyXG4gIG1hcmdpbjogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOENCNThEO1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbmlvbi1jaGVja2JveCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbi5wcml2YWN5X19wb2xpY3kge1xyXG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbn1cclxuXHJcbi5leHRfX2xpbmsge1xyXG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbiAgY29sb3I6ICMzODgwZmY7XHJcbn1cclxuXHJcbi5za2lwIHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzg4MGZmO1xyXG4gIC0tYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIC0tcGFkZGluZy10b3A6IDA7XHJcbiAgLS1wYWRkaW5nLWJvdHRvbTogMDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDEuMWVtO1xyXG4gIC0tcGFkZGluZy1lbmQ6IDEuMWVtO1xyXG4gIC0tdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyODBtcyBjdWJpYy1iZXppZXIoMC40LCAwLCAwLjIsIDEpLFxyXG4gICAgYmFja2dyb3VuZC1jb2xvciAxNW1zIGxpbmVhciwgY29sb3IgMTVtcyBsaW5lYXI7XHJcbn1cclxuXHJcbmlvbi1pbnB1dCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRlcm1zX3dyYXBwZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBmb250LXNpemU6IDExcHg7XHJcbiAgbWFyZ2luOiAxZW0gMDtcclxuICBhIHtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICB9XHJcbn1cclxuXHJcbi50ZXJtc19jaGVja2JveCB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLmFscmVhZHlfaGF2ZV9hY2NvdW50IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogMjVweDtcclxufVxyXG5cclxuLnNpZ24tdXAtYnV0dG9uLXJvdyB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogMWVtIDA7XHJcbn1cclxuXHJcbi5zaWduLXVwLWJ1dHRvbiB7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAwO1xyXG4gIC0tcGFkZGluZy1zdGFydDogMTV2dztcclxuICAtLXBhZGRpbmctZW5kOiAxNXZ3O1xyXG59XHJcblxyXG4uaW9uLXRvb2xiYXItY3VzdG9tIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5oYXZlX2FjY291bnRfbGluayB7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbiIsImlvbi1pY29uIHtcbiAgbWFyZ2luOiA1cHggIWltcG9ydGFudDtcbn1cblxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBlIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XG59XG5cbmlvbi1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5pb24tZ3JpZCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XG4gIG1hcmdpbjogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzhDQjU4RDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbn1cblxuaW9uLWNoZWNrYm94IHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5wcml2YWN5X19wb2xpY3kge1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuXG4uZXh0X19saW5rIHtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgY29sb3I6ICMzODgwZmY7XG59XG5cbi5za2lwIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICMzODgwZmY7XG4gIC0tYm9yZGVyLXJhZGl1czogNHB4O1xuICAtLXBhZGRpbmctdG9wOiAwO1xuICAtLXBhZGRpbmctYm90dG9tOiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDEuMWVtO1xuICAtLXBhZGRpbmctZW5kOiAxLjFlbTtcbiAgLS10cmFuc2l0aW9uOiBib3gtc2hhZG93IDI4MG1zIGN1YmljLWJlemllcigwLjQsIDAsIDAuMiwgMSksXG4gICAgYmFja2dyb3VuZC1jb2xvciAxNW1zIGxpbmVhciwgY29sb3IgMTVtcyBsaW5lYXI7XG59XG5cbmlvbi1pbnB1dCB7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi50ZXJtc193cmFwcGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW46IDFlbSAwO1xufVxuLnRlcm1zX3dyYXBwZXIgYSB7XG4gIGNvbG9yOiAjMDAwO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi50ZXJtc19jaGVja2JveCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5hbHJlYWR5X2hhdmVfYWNjb3VudCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cblxuLnNpZ24tdXAtYnV0dG9uLXJvdyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAxZW0gMDtcbn1cblxuLnNpZ24tdXAtYnV0dG9uIHtcbiAgLS1ib3JkZXItcmFkaXVzOiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDE1dnc7XG4gIC0tcGFkZGluZy1lbmQ6IDE1dnc7XG59XG5cbi5pb24tdG9vbGJhci1jdXN0b20ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4uaGF2ZV9hY2NvdW50X2xpbmsge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/signup/signup.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/signup/signup.page.ts ***!
  \*********************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _config_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../config/constants */ "./src/app/config/constants.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/toast.service */ "./src/app/services/toast.service.ts");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/storage.service */ "./src/app/services/storage.service.ts");
/* harmony import */ var src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/webview.service */ "./src/app/services/webview.service.ts");
/* harmony import */ var _ionic_native_spinner_dialog_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/spinner-dialog/ngx */ "./node_modules/@ionic-native/spinner-dialog/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");











let SignupPage = class SignupPage {
    constructor(router, authService, toastService, storageService, webviewService, spinnerDialog, _translate, languageService) {
        this.router = router;
        this.authService = authService;
        this.toastService = toastService;
        this.storageService = storageService;
        this.webviewService = webviewService;
        this.spinnerDialog = spinnerDialog;
        this._translate = _translate;
        this.languageService = languageService;
        this.postData = {
            profile_id: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].PROFILEID,
            api_key: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].APIKEY,
            first_name: "",
            last_name: "",
            usermail: "",
            userphone: "",
            password: "",
            app_version: "",
            request_operation: "user_register",
            language: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].LANGUAGE,
            gdpr_consent: 1,
        };
        this.isChecked = true;
        this.loading = false;
        this.languages = [
            ["en", "English"],
            ["es", "Spanish"],
            ["fi", "Finnish"],
            ["fr", "French"],
        ];
        this.getDeviceLanguage();
    }
    ngOnInit() {
        this.webviewService.loading.subscribe((value) => {
            this.loading = value;
            if (value) {
                this.spinnerDialog.show(null, this._translate.instant("Loading_Message"), true);
            }
            else {
                this.spinnerDialog.hide();
            }
        });
        this.languageService.languageCode$.subscribe((res) => {
            if (res) {
                this.language = res;
                this._initTranslate(res);
            }
        });
    }
    goToLogIn() {
        this.router.navigate(["/login"]);
    }
    goToPrivacyLink() {
        window.open(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].PRIVACYLINK);
    }
    goToTermsLink() {
        window.open(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].TERMSLINK);
    }
    validateInputs() {
        console.log(this.postData);
        let first_name = this.postData.first_name.trim();
        let last_name = this.postData.last_name.trim();
        let usermail = this.postData.usermail.trim();
        let userphone = this.postData.userphone.trim();
        let password = this.postData.password.trim();
        return (first_name &&
            last_name &&
            usermail &&
            userphone &&
            password &&
            first_name.length > 0 &&
            last_name.length > 0 &&
            usermail.length > 0 &&
            userphone.length > 0 &&
            password.length > 0);
    }
    signup() {
        if (this.validateInputs()) {
            this.authService.signup(this.postData).subscribe((res) => {
                console.log(res);
                if (res.success == "true") {
                    this.toastService.presentToast(this._translate.instant("Signup_Success"));
                    this.router.navigate(["login"]);
                }
                else {
                    this.toastService.presentToast(this._translate.instant("Response_Message", {
                        value: res.error.message,
                    }));
                }
            }, (err) => {
                console.error(err);
                this.toastService.presentToast(this._translate.instant("Error_Message"));
            });
        }
        else {
            this.toastService.presentToast(this._translate.instant("Required"));
        }
    }
    goToWebview() {
        this.webviewService.goToWebview();
    }
    changeLanguage() {
        this.languageService.setStoredLanguage(this.language);
        this._translateLanguage();
    }
    _translateLanguage() {
        this._translate.use(this.language);
    }
    _initTranslate(language) {
        // Set the default language for translation strings, and the current language.
        this._translate.setDefaultLang("en");
        if (language) {
            this.language = language;
        }
        else {
            // Set your language here
            this.language = "en";
        }
        this._translateLanguage();
    }
    getDeviceLanguage() {
        if (window.Intl && typeof window.Intl === "object") {
            if (navigator.language.split("-").length > 0) {
                const detectedLanguage = navigator.language.split("-")[0];
                this._initTranslate(detectedLanguage);
            }
        }
    }
};
SignupPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_7__["WebviewService"] },
    { type: _ionic_native_spinner_dialog_ngx__WEBPACK_IMPORTED_MODULE_8__["SpinnerDialog"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_10__["LanguageService"] }
];
SignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-signup",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./signup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/signup/signup.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./signup.page.scss */ "./src/app/pages/signup/signup.page.scss")).default]
    })
], SignupPage);



/***/ })

}]);
//# sourceMappingURL=pages-signup-signup-module-es2015.js.map