function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-dashboard-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesDashboardDashboardPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content>\r\n  <div class=\"center\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col class=\"dashboard-bg-col\"\r\n          ><ion-img\r\n            class=\"logo__image\"\r\n            src=\"../../../assets/images/dashboard-bg.png\"\r\n            [attr.alt]=\"'Restaurant_Logo' | translate\"\r\n          ></ion-img\r\n        ></ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"sign-up-button-row\">\r\n        <ion-col class=\"ion-text-center dashboard\">\r\n          <ion-button\r\n            size=\"full\"\r\n            fill=\"solid\"\r\n            size=\"large\"\r\n            class=\"order__now__button sign-up-button\"\r\n            (click)=\"goToWebview()\"\r\n            translate\r\n            >Order_Now</ion-button\r\n          >\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </div>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/pages/dashboard/dashboard.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
    \*****************************************************/

  /*! exports provided: HttpLoaderFactory, DashboardPageModule */

  /***/
  function srcAppPagesDashboardDashboardModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function () {
      return HttpLoaderFactory;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function () {
      return DashboardPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./dashboard.page */
    "./src/app/pages/dashboard/dashboard.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ngx-translate/http-loader */
    "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");

    function HttpLoaderFactory(http) {
      return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__["TranslateHttpLoader"](http, "./assets/i18n/", ".json");
    }

    var routes = [{
      path: "",
      component: _dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]
    }];

    var DashboardPageModule = function DashboardPageModule() {
      _classCallCheck(this, DashboardPageModule);
    };

    DashboardPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild({
        loader: {
          provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
          useFactory: HttpLoaderFactory,
          deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]]
        }
      })],
      declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
    })], DashboardPageModule);
    /***/
  },

  /***/
  "./src/app/pages/dashboard/dashboard.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/pages/dashboard/dashboard.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesDashboardDashboardPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".center {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  height: 100%;\n}\n\nion-content {\n  --background: var(--ion-background, #000000);\n}\n\ndiv {\n  padding: 20% 0;\n}\n\nh1 {\n  color: var(--ion-font, #ffffff);\n}\n\n.dashboard {\n  padding: 10% 0;\n}\n\nion-header ion-toolbar:first-of-type {\n  --background: var(--ion-background, #000000);\n  color: var(--ion-font, #ffffff);\n}\n\nion-header {\n  margin-bottom: 0;\n}\n\n.order__now__button {\n  --padding-start: 10px;\n}\n\n.logo__image {\n  box-shadow: 0px 0px 20px 20px #ffffff;\n  border-radius: 4px;\n}\n\n.order__now__thumbnail {\n  width: 36px;\n  -o-object-fit: contain;\n     object-fit: contain;\n  margin-right: 5px;\n}\n\n.sign-up-button-row {\n  text-align: center;\n  margin: 0;\n}\n\n.sign-up-button {\n  --border-radius: 0;\n  --padding-start: 15vw;\n  --padding-end: 15vw;\n}\n\n.dashboard-bg-col {\n  padding: 0;\n  margin-bottom: -50px;\n}\n\nion-grid {\n  padding: 0;\n  margin-top: -45px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkL0M6XFxVc2Vyc1xceW9nZXNcXE9uZURyaXZlXFxEZXNrdG9wXFxwaXp6cGlhcmV0c2FmdGVybm9jb2xvclxccGl6emFwaXJhdGVzLTIwMjMwNTE4VDE2MjAyOVotMDAxXFxwaXp6YXBpcmF0ZXMvc3JjXFxhcHBcXHBhZ2VzXFxkYXNoYm9hcmRcXGRhc2hib2FyZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNDRjs7QURDQTtFQUNFLDRDQUFBO0FDRUY7O0FEQ0E7RUFDRSxjQUFBO0FDRUY7O0FEQ0E7RUFDRSwrQkFBQTtBQ0VGOztBRENBO0VBQ0UsY0FBQTtBQ0VGOztBRENBO0VBQ0UsNENBQUE7RUFDQSwrQkFBQTtBQ0VGOztBREVBO0VBQ0UsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLHFCQUFBO0FDQ0Y7O0FERUE7RUFFRSxxQ0FBQTtFQUNBLGtCQUFBO0FDQUY7O0FER0E7RUFDRSxXQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtFQUNBLGlCQUFBO0FDQUY7O0FER0E7RUFDRSxrQkFBQTtFQUNBLFNBQUE7QUNBRjs7QURHQTtFQUNFLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtBQ0FGOztBREdBO0VBQ0UsVUFBQTtFQUNBLG9CQUFBO0FDQUY7O0FER0E7RUFDRSxVQUFBO0VBQ0EsaUJBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNlbnRlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbmlvbi1jb250ZW50IHtcclxuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLCAjMDAwMDAwKTtcclxufVxyXG5cclxuZGl2IHtcclxuICBwYWRkaW5nOiAyMCUgMDtcclxufVxyXG5cclxuaDEge1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XHJcbn1cclxuXHJcbi5kYXNoYm9hcmQge1xyXG4gIHBhZGRpbmc6IDEwJSAwO1xyXG59XHJcblxyXG5pb24taGVhZGVyIGlvbi10b29sYmFyOmZpcnN0LW9mLXR5cGUge1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XHJcbiAgLy8gYm94LXNoYWRvdzogMHB4IDJweCAyMHB4IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taGVhZGVyIHtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcblxyXG4ub3JkZXJfX25vd19fYnV0dG9uIHtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XHJcbn1cclxuXHJcbi5sb2dvX19pbWFnZSB7XHJcbiAgLy8gb3BhY2l0eTogMC41O1xyXG4gIGJveC1zaGFkb3c6IDBweCAwcHggMjBweCAyMHB4ICNmZmZmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG4ub3JkZXJfX25vd19fdGh1bWJuYWlsIHtcclxuICB3aWR0aDogMzZweDtcclxuICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG4uc2lnbi11cC1idXR0b24tcm93IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4uc2lnbi11cC1idXR0b24ge1xyXG4gIC0tYm9yZGVyLXJhZGl1czogMDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDE1dnc7XHJcbiAgLS1wYWRkaW5nLWVuZDogMTV2dztcclxufVxyXG5cclxuLmRhc2hib2FyZC1iZy1jb2wge1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgbWFyZ2luLWJvdHRvbTogLTUwcHg7XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbi10b3A6IC00NXB4O1xyXG59XHJcbiIsIi5jZW50ZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xufVxuXG5kaXYge1xuICBwYWRkaW5nOiAyMCUgMDtcbn1cblxuaDEge1xuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xufVxuXG4uZGFzaGJvYXJkIHtcbiAgcGFkZGluZzogMTAlIDA7XG59XG5cbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xufVxuXG5pb24taGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cblxuLm9yZGVyX19ub3dfX2J1dHRvbiB7XG4gIC0tcGFkZGluZy1zdGFydDogMTBweDtcbn1cblxuLmxvZ29fX2ltYWdlIHtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAyMHB4IDIwcHggI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuXG4ub3JkZXJfX25vd19fdGh1bWJuYWlsIHtcbiAgd2lkdGg6IDM2cHg7XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uc2lnbi11cC1idXR0b24tcm93IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG59XG5cbi5zaWduLXVwLWJ1dHRvbiB7XG4gIC0tYm9yZGVyLXJhZGl1czogMDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNXZ3O1xuICAtLXBhZGRpbmctZW5kOiAxNXZ3O1xufVxuXG4uZGFzaGJvYXJkLWJnLWNvbCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbi1ib3R0b206IC01MHB4O1xufVxuXG5pb24tZ3JpZCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbi10b3A6IC00NXB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/dashboard/dashboard.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/dashboard/dashboard.page.ts ***!
    \***************************************************/

  /*! exports provided: DashboardPage */

  /***/
  function srcAppPagesDashboardDashboardPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardPage", function () {
      return DashboardPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _services_dashboard_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./../../services/dashboard.service */
    "./src/app/services/dashboard.service.ts");
    /* harmony import */


    var _services_toast_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./../../services/toast.service */
    "./src/app/services/toast.service.ts");
    /* harmony import */


    var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/services/webview.service */
    "./src/app/services/webview.service.ts");
    /* harmony import */


    var _ionic_native_spinner_dialog_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/spinner-dialog/ngx */
    "./node_modules/@ionic-native/spinner-dialog/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/services/language.service */
    "./src/app/services/language.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

    var DashboardPage = /*#__PURE__*/function () {
      function DashboardPage(dashboardService, toastService, router, authService, webviewService, spinnerDialog, languageService, _translate) {
        _classCallCheck(this, DashboardPage);

        this.dashboardService = dashboardService;
        this.toastService = toastService;
        this.router = router;
        this.authService = authService;
        this.webviewService = webviewService;
        this.spinnerDialog = spinnerDialog;
        this.languageService = languageService;
        this._translate = _translate;
        this.loading = false;
      }

      _createClass(DashboardPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.authService.userData$.subscribe(function (res) {
            console.log(res);
            _this.authUser = res;
          });
          this.languageService.languageCode$.subscribe(function (res) {
            if (res) {
              _this.language = res;

              _this._initTranslate(res);
            }
          });
          this.webviewService.loading.subscribe(function (value) {
            console.log("LOADING STATE", value);
            _this.loading = value;

            if (value) {
              _this.spinnerDialog.show(null, _this._translate.instant("Loading_Message"), true);
            } else {
              _this.spinnerDialog.hide();
            }
          });
        }
      }, {
        key: "goToInfo",
        value: function goToInfo() {
          window.open("https://www.kochloeffel.jp/aboutus");
        }
      }, {
        key: "goToDSTY",
        value: function goToDSTY() {
          window.open("https://www.dstymensa.com");
        }
      }, {
        key: "goToWebview",
        value: function goToWebview() {
          this.webviewService.goToWebview(this.authUser.session_id);
        }
      }, {
        key: "_translateLanguage",
        value: function _translateLanguage() {
          this._translate.use(this.language);
        }
      }, {
        key: "_initTranslate",
        value: function _initTranslate(language) {
          // Set the default language for translation strings, and the current language.
          this._translate.setDefaultLang("en");

          if (language) {
            this.language = language;
          } else {
            // Set your language here
            this.language = "en";
          }

          this._translateLanguage();
        }
      }]);

      return DashboardPage;
    }();

    DashboardPage.ctorParameters = function () {
      return [{
        type: _services_dashboard_service__WEBPACK_IMPORTED_MODULE_2__["DashboardService"]
      }, {
        type: _services_toast_service__WEBPACK_IMPORTED_MODULE_3__["ToastService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
      }, {
        type: src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_6__["WebviewService"]
      }, {
        type: _ionic_native_spinner_dialog_ngx__WEBPACK_IMPORTED_MODULE_7__["SpinnerDialog"]
      }, {
        type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_8__["LanguageService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"]
      }];
    };

    DashboardPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-dashboard",
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./dashboard.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./dashboard.page.scss */
      "./src/app/pages/dashboard/dashboard.page.scss"))["default"]]
    })], DashboardPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-dashboard-dashboard-module-es5.js.map