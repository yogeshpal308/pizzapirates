(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/slides/slides.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/slides/slides.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-slides pager=\"true\" [options]=\"slideOpts\">\r\n  <ion-slide>\r\n    <img src=\"assets/images/logo.png\" />\r\n    <h3>A Powerful Online Ordering System Built For Your Restaurant</h3>\r\n    <p>Experience more orders, bigger order sizes and more revenues!</p>\r\n  </ion-slide>\r\n  <ion-slide>\r\n    <h2>The Power Packed Features Of Restolabs To Amplify Your Business</h2>\r\n    <ion-list>\r\n      <ion-item>\r\n        <ion-label>QUICK MENU SET UP</ion-label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>EASY ORDER MANAGEMENT</ion-label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>MULTILINGUAL SUPPORT</ion-label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>INBUILT MARKETING TOOLS</ion-label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>ANALYTICS</ion-label>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>DEDICATED SUPPORT</ion-label>\r\n      </ion-item>\r\n    </ion-list>\r\n  </ion-slide>\r\n</ion-slides>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/start-button/start-button.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/start-button/start-button.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"fixed-bottom\">\r\n  <ion-footer>\r\n    <ion-toolbar class=\"ion-text-end\">\r\n      <ion-button shape=\"round\" color=\"warning\" (click)=\"navigateToLogin()\">\r\n        Start\r\n        <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-toolbar>\r\n  </ion-footer>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen class=\"ion-no-padding\">\r\n  <ion-header translucent>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button\r\n          defaultHref=\"home\"\r\n          text=\"\"\r\n          icon=\"chevron-back-outline\"\r\n        ></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title translate>\r\n        Profile\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <ion-grid class=\"ion-padding\">\r\n    <ion-row>\r\n      <ion-col class=\"dashboard-bg-col\"\r\n        ><ion-img\r\n          class=\"logo__image\"\r\n          src=\"../../../assets/images/profile-bg.png\"\r\n          [attr.alt]=\"'Restaurant_Logo' | translate\"\r\n        ></ion-img\r\n      ></ion-col>\r\n    </ion-row>\r\n    <div class=\"profile_content\">\r\n      <ion-row *ngIf=\"authUser?.user_info.first_name\">\r\n        <ion-col translate>Name</ion-col>\r\n        <ion-col size=\"8\"\r\n          >{{authUser?.user_info.first_name}}&nbsp;{{authUser?.user_info.last_name}}</ion-col\r\n        >\r\n      </ion-row>\r\n\r\n      <ion-row *ngIf=\"authUser?.user_info.mail\">\r\n        <ion-col translate>Email</ion-col>\r\n        <ion-col size=\"8\">{{authUser?.user_info.mail}}</ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row *ngIf=\"authUser?.user_info.phone\">\r\n        <ion-col translate>Mobile_Number</ion-col>\r\n        <ion-col size=\"8\">{{authUser?.user_info.phone}}</ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row>\r\n        <ion-col>\r\n          <span class=\"logout_button\" (click)=\"logoutAction()\" translate\r\n            >Logout</span\r\n          >\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/components/components.module.ts":
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _slides_slides_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./slides/slides.component */ "./src/app/components/slides/slides.component.ts");
/* harmony import */ var _start_button_start_button_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./start-button/start-button.component */ "./src/app/components/start-button/start-button.component.ts");







let ComponentsModule = class ComponentsModule {
};
ComponentsModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [_slides_slides_component__WEBPACK_IMPORTED_MODULE_5__["SlidesComponent"], _start_button_start_button_component__WEBPACK_IMPORTED_MODULE_6__["StartButtonComponent"]],
        exports: [_slides_slides_component__WEBPACK_IMPORTED_MODULE_5__["SlidesComponent"], _start_button_start_button_component__WEBPACK_IMPORTED_MODULE_6__["StartButtonComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"]],
    })
], ComponentsModule);



/***/ }),

/***/ "./src/app/components/slides/slides.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/slides/slides.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-slide {\n  padding: 50px 20px;\n  display: block;\n}\nion-slide img {\n  width: 300px;\n}\nion-slide h3 {\n  font-size: 24px;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zbGlkZXMvQzpcXFVzZXJzXFx5b2dlc1xcT25lRHJpdmVcXERlc2t0b3BcXHBpenpwaWFyZXRzYWZ0ZXJub2NvbG9yXFxwaXp6YXBpcmF0ZXMtMjAyMzA1MThUMTYyMDI5Wi0wMDFcXHBpenphcGlyYXRlcy9zcmNcXGFwcFxcY29tcG9uZW50c1xcc2xpZGVzXFxzbGlkZXMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvc2xpZGVzL3NsaWRlcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtBQ0NGO0FEQUU7RUFDRSxZQUFBO0FDRUo7QURDRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zbGlkZXMvc2xpZGVzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXNsaWRlIHtcclxuICBwYWRkaW5nOiA1MHB4IDIwcHg7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcblxyXG4gIGgzIHtcclxuICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxufVxyXG4iLCJpb24tc2xpZGUge1xuICBwYWRkaW5nOiA1MHB4IDIwcHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuaW9uLXNsaWRlIGltZyB7XG4gIHdpZHRoOiAzMDBweDtcbn1cbmlvbi1zbGlkZSBoMyB7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/slides/slides.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/slides/slides.component.ts ***!
  \*******************************************************/
/*! exports provided: SlidesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlidesComponent", function() { return SlidesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let SlidesComponent = class SlidesComponent {
    constructor() {
        this.slideOpts = {
            speed: 400
        };
    }
};
SlidesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-slides',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./slides.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/slides/slides.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./slides.component.scss */ "./src/app/components/slides/slides.component.scss")).default]
    })
], SlidesComponent);



/***/ }),

/***/ "./src/app/components/start-button/start-button.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/start-button/start-button.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-button {\n  --padding-start: 15px;\n  --padding-end: 15px;\n}\n\n.fixed-bottom {\n  position: fixed;\n  bottom: 0px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zdGFydC1idXR0b24vQzpcXFVzZXJzXFx5b2dlc1xcT25lRHJpdmVcXERlc2t0b3BcXHBpenpwaWFyZXRzYWZ0ZXJub2NvbG9yXFxwaXp6YXBpcmF0ZXMtMjAyMzA1MThUMTYyMDI5Wi0wMDFcXHBpenphcGlyYXRlcy9zcmNcXGFwcFxcY29tcG9uZW50c1xcc3RhcnQtYnV0dG9uXFxzdGFydC1idXR0b24uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvc3RhcnQtYnV0dG9uL3N0YXJ0LWJ1dHRvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHFCQUFBO0VBQ0EsbUJBQUE7QUNDRjs7QURFQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zdGFydC1idXR0b24vc3RhcnQtYnV0dG9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWJ1dHRvbiB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNXB4O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDE1cHg7XHJcbn1cclxuXHJcbi5maXhlZC1ib3R0b20ge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBib3R0b206IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iLCJpb24tYnV0dG9uIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNXB4O1xuICAtLXBhZGRpbmctZW5kOiAxNXB4O1xufVxuXG4uZml4ZWQtYm90dG9tIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDBweDtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/start-button/start-button.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/start-button/start-button.component.ts ***!
  \*******************************************************************/
/*! exports provided: StartButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartButtonComponent", function() { return StartButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let StartButtonComponent = class StartButtonComponent {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() { }
    navigateToLogin() {
        this.router.navigate(["/login"]);
    }
};
StartButtonComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
StartButtonComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-start-button",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./start-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/start-button/start-button.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./start-button.component.scss */ "./src/app/components/start-button/start-button.component.scss")).default]
    })
], StartButtonComponent);



/***/ }),

/***/ "./src/app/pages/profile/profile.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.module.ts ***!
  \*************************************************/
/*! exports provided: HttpLoaderFactory, ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile.page */ "./src/app/pages/profile/profile.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");











function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_10__["TranslateHttpLoader"](http, "./assets/i18n/", ".json");
}
const routes = [
    {
        path: "",
        component: _profile_page__WEBPACK_IMPORTED_MODULE_7__["ProfilePage"],
    },
];
let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateModule"].forChild({
                loader: {
                    provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateLoader"],
                    useFactory: HttpLoaderFactory,
                    deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"]],
                },
            }),
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_7__["ProfilePage"]],
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/pages/profile/profile.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-background, #000000);\n}\n\nion-header ion-toolbar:first-of-type {\n  --background: var(--ion-background, #000000);\n  color: var(--ion-font, #ffffff);\n}\n\nion-grid {\n  color: var(--ion-font, #ffffff);\n  padding: 0;\n}\n\n.dashboard-bg-col {\n  padding: 0;\n}\n\n.back-icon {\n  font-size: 33px;\n}\n\n.profile_name {\n  vertical-align: super;\n}\n\n.profile_content {\n  display: flex;\n  flex-direction: column;\n  padding: 15px;\n  margin: 20px;\n  background-color: #8CB58D;\n  border-radius: 20px;\n}\n\n.logout_button {\n  font-style: italic;\n  color: #00000087;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZmlsZS9DOlxcVXNlcnNcXHlvZ2VzXFxPbmVEcml2ZVxcRGVza3RvcFxccGl6enBpYXJldHNhZnRlcm5vY29sb3JcXHBpenphcGlyYXRlcy0yMDIzMDUxOFQxNjIwMjlaLTAwMVxccGl6emFwaXJhdGVzL3NyY1xcYXBwXFxwYWdlc1xccHJvZmlsZVxccHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw0Q0FBQTtBQ0NGOztBRENBO0VBQ0UsNENBQUE7RUFDQSwrQkFBQTtBQ0VGOztBRENBO0VBQ0UsK0JBQUE7RUFDQSxVQUFBO0FDRUY7O0FEQUE7RUFDRSxVQUFBO0FDR0Y7O0FEREE7RUFDRSxlQUFBO0FDSUY7O0FERkE7RUFDRSxxQkFBQTtBQ0tGOztBRENBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDRUY7O0FEQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FDRUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xyXG59XHJcbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcclxuICAvLyBib3gtc2hhZG93OiAwcHggMnB4IDBweCB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZikgIWltcG9ydGFudDsgXHJcbn1cclxuaW9uLWdyaWQge1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG4uZGFzaGJvYXJkLWJnLWNvbCB7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG4uYmFjay1pY29uIHtcclxuICBmb250LXNpemU6IDMzcHg7XHJcbn1cclxuLnByb2ZpbGVfbmFtZXtcclxuICB2ZXJ0aWNhbC1hbGlnbjogc3VwZXI7XHJcbn1cclxuLy8gaW9uLWJ1dHRvbiB7XHJcbi8vICAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcclxuLy8gfVxyXG5cclxuLnByb2ZpbGVfY29udGVudCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIHBhZGRpbmc6IDE1cHg7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM4Q0I1OEQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG5cclxuLmxvZ291dF9idXR0b24ge1xyXG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICBjb2xvcjogIzAwMDAwMDg3O1xyXG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLCAjMDAwMDAwKTtcbn1cblxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBlIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XG59XG5cbmlvbi1ncmlkIHtcbiAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcbiAgcGFkZGluZzogMDtcbn1cblxuLmRhc2hib2FyZC1iZy1jb2wge1xuICBwYWRkaW5nOiAwO1xufVxuXG4uYmFjay1pY29uIHtcbiAgZm9udC1zaXplOiAzM3B4O1xufVxuXG4ucHJvZmlsZV9uYW1lIHtcbiAgdmVydGljYWwtYWxpZ246IHN1cGVyO1xufVxuXG4ucHJvZmlsZV9jb250ZW50IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgcGFkZGluZzogMTVweDtcbiAgbWFyZ2luOiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOENCNThEO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xufVxuXG4ubG9nb3V0X2J1dHRvbiB7XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgY29sb3I6ICMwMDAwMDA4Nztcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/profile/profile.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/profile/profile.page.ts ***!
  \***********************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var src_app_services_token_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/token.service */ "./src/app/services/token.service.ts");
/* harmony import */ var _config_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../config/constants */ "./src/app/config/constants.ts");





let ProfilePage = class ProfilePage {
    constructor(authService, tokenService) {
        this.authService = authService;
        this.tokenService = tokenService;
    }
    ngOnInit() {
        this.authService.userData$.subscribe((res) => {
            this.authUser = res;
            console.log(this.authUser);
        });
    }
    logoutAction() {
        var _a, _b;
        this.authService.logout({
            profile_id: _config_constants__WEBPACK_IMPORTED_MODULE_4__["Constants"].PROFILEID,
            user_id: (_a = this.authUser) === null || _a === void 0 ? void 0 : _a.user_id,
            api_key: _config_constants__WEBPACK_IMPORTED_MODULE_4__["Constants"].APIKEY,
            app_version: "",
            request_operation: "user_logout",
            session_id: (_b = this.authUser) === null || _b === void 0 ? void 0 : _b.session_id,
            language: _config_constants__WEBPACK_IMPORTED_MODULE_4__["Constants"].LANGUAGE,
        });
    }
};
ProfilePage.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: src_app_services_token_service__WEBPACK_IMPORTED_MODULE_3__["TokenService"] }
];
ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-profile",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile.page.scss */ "./src/app/pages/profile/profile.page.scss")).default]
    })
], ProfilePage);



/***/ })

}]);
//# sourceMappingURL=pages-profile-profile-module-es2015.js.map