function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forgot-password-forgot-password-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesForgotPasswordForgotPasswordPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content fullscreen class=\"ion-no-padding\">\r\n  <ion-header translucent>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button\r\n          defaultHref=\"/\"\r\n          text=\"\"\r\n          icon=\"chevron-back-outline\"\r\n        ></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n        <ion-button\r\n          class=\"ion-float-right\"\r\n          size=\"full\"\r\n          fill=\"solid\"\r\n          size=\"small\"\r\n          (click)=\"goToWebview()\"\r\n          translate\r\n        >\r\n          Skip_Sign_Up</ion-button\r\n        >\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <ion-img\r\n    class=\"logo__image\"\r\n    src=\"../../../assets/images/forget-pass-bg.png\"\r\n    [attr.alt]=\"'Restaurant_Logo' | translate\"\r\n  ></ion-img>\r\n  <ion-grid class=\"ion-padding\">\r\n    <ion-row>\r\n      <ion-col>\r\n        <p class=\"forget_pass_statement\" translate>Forgot_Password</p>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"mail-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"email\"\r\n            required\r\n            pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$\"\r\n            placeholder=\"{{'Email' | translate}}*\"\r\n            [(ngModel)]=\"postData.usermail\"\r\n            #usermail=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"usermail.invalid && (usermail.dirty || usermail.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"usermail.errors.required\" class=\"invalid\">\r\n            Email is required.\r\n          </p>\r\n          <p *ngIf=\"usermail.errors.pattern\" class=\"invalid\">\r\n            Please enter valid email address.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"sign-up-button-row\">\r\n      <ion-col>\r\n        <ion-button\r\n          class=\"sign-up-button\"\r\n          size=\"full\"\r\n          fill=\"solid\"\r\n          size=\"large\"\r\n          (click)=\"submit()\"\r\n          translate\r\n          >Submit</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"already_have_account\">\r\n      <ion-col>\r\n        <span class=\"have_account_link\" (click)=\"goToLogIn()\" translate\r\n          >Have_Account</span\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <ion-row>\r\n    <div class=\"terms_wrapper\">\r\n      <span translate>By_continue</span>\r\n      <span>\r\n        <a (click)=\"goToTermsLink()\" translate>Terms_Conditions</a>\r\n        <a (click)=\"goToPrivacyLink()\" translate>Privacy_Policy</a>\r\n      </span>\r\n    </div>\r\n  </ion-row>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/pages/forgot-password/forgot-password.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/forgot-password/forgot-password.module.ts ***!
    \*****************************************************************/

  /*! exports provided: HttpLoaderFactory, ForgotPasswordPageModule */

  /***/
  function srcAppPagesForgotPasswordForgotPasswordModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function () {
      return HttpLoaderFactory;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function () {
      return ForgotPasswordPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./forgot-password.page */
    "./src/app/pages/forgot-password/forgot-password.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ngx-translate/http-loader */
    "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");

    function HttpLoaderFactory(http) {
      return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__["TranslateHttpLoader"](http, "./assets/i18n/", ".json");
    }

    var routes = [{
      path: "",
      component: _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]
    }];

    var ForgotPasswordPageModule = function ForgotPasswordPageModule() {
      _classCallCheck(this, ForgotPasswordPageModule);
    };

    ForgotPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild({
        loader: {
          provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
          useFactory: HttpLoaderFactory,
          deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]]
        }
      })],
      declarations: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]]
    })], ForgotPasswordPageModule);
    /***/
  },

  /***/
  "./src/app/pages/forgot-password/forgot-password.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/forgot-password/forgot-password.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesForgotPasswordForgotPasswordPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-icon {\n  margin: 5px !important;\n}\n\nion-header ion-toolbar:first-of-type {\n  --background: var(--ion-background, #000000);\n  color: var(--ion-font, #ffffff);\n}\n\nion-content {\n  --background: var(--ion-background, #000000);\n}\n\nion-item {\n  border-radius: 12px;\n  border: none;\n}\n\nion-grid {\n  color: var(--ion-font, #ffffff);\n  margin: 20px;\n  background-color: #8CB58D;\n  border-radius: 20px;\n}\n\nion-input {\n  margin-left: 15px !important;\n}\n\n.terms_wrapper {\n  display: flex;\n  flex-flow: column;\n  justify-content: center;\n  width: 100%;\n  align-items: center;\n  font-size: 11px;\n  margin: 1em 0;\n}\n\n.terms_wrapper a {\n  color: #000;\n  margin-right: 5px;\n  text-decoration: underline;\n}\n\n.terms_checkbox {\n  display: none;\n}\n\n.already_have_account {\n  text-align: center;\n  margin-top: 25px;\n}\n\n.sign-up-button-row {\n  text-align: center;\n  margin: 1em 0;\n}\n\n.sign-up-button {\n  --border-radius: 0;\n  --padding-start: 15vw;\n  --padding-end: 15vw;\n}\n\n.ion-toolbar-custom {\n  display: flex;\n  padding-right: 10px;\n}\n\n.have_account_link {\n  cursor: pointer;\n}\n\n.forget_pass_statement {\n  padding-left: 15px;\n}\n\n.back-icon {\n  font-size: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm9yZ290LXBhc3N3b3JkL0M6XFxVc2Vyc1xceW9nZXNcXE9uZURyaXZlXFxEZXNrdG9wXFxwaXp6cGlhcmV0c2FmdGVybm9jb2xvclxccGl6emFwaXJhdGVzLTIwMjMwNTE4VDE2MjAyOVotMDAxXFxwaXp6YXBpcmF0ZXMvc3JjXFxhcHBcXHBhZ2VzXFxmb3Jnb3QtcGFzc3dvcmRcXGZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usc0JBQUE7QUNDRjs7QURFQTtFQUNFLDRDQUFBO0VBQ0EsK0JBQUE7QUNDRjs7QURHQTtFQUNFLDRDQUFBO0FDQUY7O0FER0E7RUFDRSxtQkFBQTtFQUNBLFlBQUE7QUNBRjs7QURLQTtFQUNFLCtCQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUNGRjs7QURLQTtFQUNFLDRCQUFBO0FDRkY7O0FES0E7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0FDRkY7O0FER0U7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtBQ0RKOztBREtBO0VBQ0UsYUFBQTtBQ0ZGOztBREtBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBQ0ZGOztBREtBO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0FDRkY7O0FES0E7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7QUNGRjs7QURLQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQ0ZGOztBREtBO0VBQ0UsZUFBQTtBQ0ZGOztBREtBO0VBQ0Usa0JBQUE7QUNGRjs7QURLQTtFQUNFLGVBQUE7QUNGRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWljb24ge1xyXG4gIG1hcmdpbjogNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcclxuICAvLyBib3gtc2hhZG93OiAwcHggMnB4IDBweCB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZikgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xyXG59XHJcblxyXG5pb24taXRlbSB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuLy8gaW9uLWJ1dHRvbiB7XHJcbi8vICAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcclxuLy8gfVxyXG5pb24tZ3JpZCB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcclxuICBtYXJnaW46IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzhDQjU4RDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG5pb24taW5wdXQge1xyXG4gIG1hcmdpbi1sZWZ0OiAxNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXJtc193cmFwcGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIG1hcmdpbjogMWVtIDA7XHJcbiAgYSB7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgfVxyXG59XHJcblxyXG4udGVybXNfY2hlY2tib3gge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi5hbHJlYWR5X2hhdmVfYWNjb3VudCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDI1cHg7XHJcbn1cclxuXHJcbi5zaWduLXVwLWJ1dHRvbi1yb3cge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW46IDFlbSAwO1xyXG59XHJcblxyXG4uc2lnbi11cC1idXR0b24ge1xyXG4gIC0tYm9yZGVyLXJhZGl1czogMDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDE1dnc7XHJcbiAgLS1wYWRkaW5nLWVuZDogMTV2dztcclxufVxyXG5cclxuLmlvbi10b29sYmFyLWN1c3RvbSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG4uaGF2ZV9hY2NvdW50X2xpbmsge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmZvcmdldF9wYXNzX3N0YXRlbWVudCB7XHJcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG59XHJcblxyXG4uYmFjay1pY29uIHtcclxuICBmb250LXNpemU6IDI0cHg7XHJcbn0iLCJpb24taWNvbiB7XG4gIG1hcmdpbjogNXB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xufVxuXG5pb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuaW9uLWdyaWQge1xuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xuICBtYXJnaW46IDIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM4Q0I1OEQ7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG59XG5cbmlvbi1pbnB1dCB7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi50ZXJtc193cmFwcGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW46IDFlbSAwO1xufVxuLnRlcm1zX3dyYXBwZXIgYSB7XG4gIGNvbG9yOiAjMDAwO1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi50ZXJtc19jaGVja2JveCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5hbHJlYWR5X2hhdmVfYWNjb3VudCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjVweDtcbn1cblxuLnNpZ24tdXAtYnV0dG9uLXJvdyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAxZW0gMDtcbn1cblxuLnNpZ24tdXAtYnV0dG9uIHtcbiAgLS1ib3JkZXItcmFkaXVzOiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDE1dnc7XG4gIC0tcGFkZGluZy1lbmQ6IDE1dnc7XG59XG5cbi5pb24tdG9vbGJhci1jdXN0b20ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4uaGF2ZV9hY2NvdW50X2xpbmsge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5mb3JnZXRfcGFzc19zdGF0ZW1lbnQge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG5cbi5iYWNrLWljb24ge1xuICBmb250LXNpemU6IDI0cHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/forgot-password/forgot-password.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/forgot-password/forgot-password.page.ts ***!
    \***************************************************************/

  /*! exports provided: ForgotPasswordPage */

  /***/
  function srcAppPagesForgotPasswordForgotPasswordPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function () {
      return ForgotPasswordPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _config_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../config/constants */
    "./src/app/config/constants.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./../../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _services_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./../../services/storage.service */
    "./src/app/services/storage.service.ts");
    /* harmony import */


    var _services_toast_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./../../services/toast.service */
    "./src/app/services/toast.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/services/webview.service */
    "./src/app/services/webview.service.ts");

    var ForgotPasswordPage = /*#__PURE__*/function () {
      function ForgotPasswordPage(router, authService, storageService, webviewService, toastService, _translate) {
        _classCallCheck(this, ForgotPasswordPage);

        this.router = router;
        this.authService = authService;
        this.storageService = storageService;
        this.webviewService = webviewService;
        this.toastService = toastService;
        this._translate = _translate;
        this.postData = {
          profile_id: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].PROFILEID,
          api_key: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].APIKEY,
          usermail: "",
          app_version: "",
          request_operation: "user_password_reset_mail",
          language: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].LANGUAGE
        };
        this.loading = false;
      }

      _createClass(ForgotPasswordPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goToWebview",
        value: function goToWebview() {
          this.webviewService.goToWebview();
        }
      }, {
        key: "goToLogIn",
        value: function goToLogIn() {
          this.router.navigate(["/login"]);
        }
      }, {
        key: "goToPrivacyLink",
        value: function goToPrivacyLink() {
          window.open(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].PRIVACYLINK);
        }
      }, {
        key: "goToTermsLink",
        value: function goToTermsLink() {
          window.open(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].TERMSLINK);
        }
      }, {
        key: "validateInputs",
        value: function validateInputs() {
          console.log(this.postData);
          var usermail = this.postData.usermail.trim();
          return this.postData.usermail && usermail.length > 0;
        }
      }, {
        key: "submit",
        value: function submit() {
          var _this = this;

          if (this.validateInputs()) {
            this.authService.login(this.postData).subscribe(function (res) {
              console.log(res);

              if (res.success == "true") {
                _this.toastService.presentToast(res.response.message);

                _this.toastService.presentToast(_this._translate.instant("Response_Message", {
                  value: res.response.message
                }));
              } else {
                _this.toastService.presentToast(_this._translate.instant("Response_Message", {
                  value: res.error.message
                }));
              }
            }, function (err) {
              console.log(err);

              _this.toastService.presentToast(_this._translate.instant("Error_Message"));
            });
          } else {
            this.toastService.presentToast(this._translate.instant("Invalid_Email"));
          }
        }
      }]);

      return ForgotPasswordPage;
    }();

    ForgotPasswordPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
      }, {
        type: _services_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"]
      }, {
        type: src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_8__["WebviewService"]
      }, {
        type: _services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"]
      }];
    };

    ForgotPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-forgot-password",
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./forgot-password.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./forgot-password.page.scss */
      "./src/app/pages/forgot-password/forgot-password.page.scss"))["default"]]
    })], ForgotPasswordPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-forgot-password-forgot-password-module-es5.js.map