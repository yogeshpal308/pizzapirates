(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content fullscreen class=\"ion-no-padding\">\r\n  <ion-header translucent>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button\r\n          defaultHref=\"/\"\r\n          text=\"\"\r\n          icon=\"chevron-back-outline\"\r\n        ></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n        <ion-button\r\n          class=\"ion-float-right\"\r\n          size=\"full\"\r\n          fill=\"solid\"\r\n          size=\"small\"\r\n          (click)=\"goToWebview()\"\r\n          translate\r\n        >\r\n          Skip_Sign_Up</ion-button\r\n        >\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <ion-img\r\n    class=\"logo__image\"\r\n    src=\"../../../assets/images/login-bg.png\"\r\n    [attr.alt]=\"'Restaurant_Logo' | translate\"\r\n  ></ion-img>\r\n\r\n  <ion-grid class=\"ion-padding\">\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"mail-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"email\"\r\n            required\r\n            pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$\"\r\n            placeholder=\"{{'Email' | translate}}*\"\r\n            [(ngModel)]=\"postData.usermail\"\r\n            #usermail=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"usermail.invalid && (usermail.dirty || usermail.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"usermail.errors.required\" class=\"invalid\">\r\n            Email is required.\r\n          </p>\r\n          <p *ngIf=\"usermail.errors.pattern\" class=\"invalid\">\r\n            Please enter valid email address.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item class=\"ion-no-padding\">\r\n          <!-- <ion-icon name=\"lock-closed-outline\" slot=\"start\"></ion-icon> -->\r\n          <ion-input\r\n            autocomplete=\"off\"\r\n            type=\"password\"\r\n            required\r\n            minlength=\"6\"\r\n            placeholder=\"{{'Password' | translate}}*\"\r\n            [(ngModel)]=\"postData.password\"\r\n            #password=\"ngModel\"\r\n          >\r\n          </ion-input>\r\n        </ion-item>\r\n        <div\r\n          *ngIf=\"password.invalid && (password.dirty || password.touched)\"\r\n          class=\"alert alert-danger\"\r\n        >\r\n          <p *ngIf=\"password.errors.required\" class=\"invalid\">\r\n            Password is required.\r\n          </p>\r\n          <p *ngIf=\"password.errors.minlength\" class=\"invalid\">\r\n            Password must be at least 6 characters long.\r\n          </p>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"sign-up-button-row\">\r\n      <ion-col>\r\n        <ion-button\r\n          class=\"sign-up-button\"\r\n          size=\"full\"\r\n          fill=\"solid\"\r\n          size=\"large\"\r\n          (click)=\"loginAction()\"\r\n          translate\r\n          >sign_in</ion-button\r\n        >\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n      <div class=\"sign-login-row\">\r\n        <span (click)=\"goToForgotPassword()\" translate\r\n          >Forgot_Password<span>?</span></span\r\n        >\r\n        <span (click)=\"goToSignUp()\" translate\r\n          >Create_Account<span>?</span></span\r\n        >\r\n      </div>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <ion-row>\r\n    <div class=\"terms_wrapper\">\r\n      <span translate>By_continue</span>\r\n      <span>\r\n        <a (click)=\"goToTermsLink()\" translate>Terms_Conditions</a>\r\n        <a (click)=\"goToPrivacyLink()\" translate>Privacy_Policy</a>\r\n      </span>\r\n    </div>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: HttpLoaderFactory, LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");










function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_9__["TranslateHttpLoader"](http, "./assets/i18n/", ".json");
}
const routes = [
    {
        path: "",
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"],
    },
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild({
                loader: {
                    provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
                    useFactory: HttpLoaderFactory,
                    deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]],
                },
            }),
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-icon {\n  margin: 5px !important;\n}\n\nion-header ion-toolbar:first-of-type {\n  --background: var(--ion-background, #000000);\n  color: var(--ion-font, #ffffff);\n}\n\nion-content {\n  --background: var(--ion-background, #000000);\n}\n\nion-item {\n  border-radius: 12px;\n  border: none;\n}\n\nion-grid {\n  color: var(--ion-font, #ffffff);\n  margin: 20px;\n  background-color: #8CB58D;\n  border-radius: 20px;\n}\n\nion-input {\n  margin-left: 15px !important;\n}\n\n.terms_wrapper {\n  display: flex;\n  flex-flow: column;\n  justify-content: center;\n  width: 100%;\n  align-items: center;\n  font-size: 11px;\n  margin: 1em 0;\n}\n\n.terms_wrapper a {\n  color: #000;\n  margin-right: 5px;\n  text-decoration: underline;\n}\n\n.terms_checkbox {\n  display: none;\n}\n\n.already_have_account {\n  text-align: center;\n  margin-top: 25px;\n}\n\n.sign-up-button-row {\n  text-align: center;\n  margin: 1em 0;\n}\n\n.sign-up-button {\n  --border-radius: 0;\n  --padding-start: 15vw;\n  --padding-end: 15vw;\n}\n\n.ion-toolbar-custom {\n  display: flex;\n  padding-right: 10px;\n}\n\n.have_account_link {\n  cursor: pointer;\n}\n\n.forget_pass_statement {\n  padding-left: 15px;\n}\n\n.back-icon {\n  font-size: 24px;\n}\n\n.sign-login-row {\n  display: flex;\n  flex-flow: column;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n\n.sign-login-row span {\n  margin: 10px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXFVzZXJzXFx5b2dlc1xcT25lRHJpdmVcXERlc2t0b3BcXHBpenpwaWFyZXRzYWZ0ZXJub2NvbG9yXFxwaXp6YXBpcmF0ZXMtMjAyMzA1MThUMTYyMDI5Wi0wMDFcXHBpenphcGlyYXRlcy9zcmNcXGFwcFxccGFnZXNcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFBO0FDQ0Y7O0FERUE7RUFDRSw0Q0FBQTtFQUNBLCtCQUFBO0FDQ0Y7O0FER0E7RUFDRSw0Q0FBQTtBQ0FGOztBREdBO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0FDQUY7O0FESUE7RUFDRSwrQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDREY7O0FESUE7RUFDRSw0QkFBQTtBQ0RGOztBREdBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtBQ0FGOztBRENFO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsMEJBQUE7QUNDSjs7QURHQTtFQUNFLGFBQUE7QUNBRjs7QURHQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUNBRjs7QURHQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtBQ0FGOztBREdBO0VBQ0Usa0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDQUY7O0FER0E7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUNBRjs7QURHQTtFQUNFLGVBQUE7QUNBRjs7QURHQTtFQUNFLGtCQUFBO0FDQUY7O0FER0E7RUFDRSxlQUFBO0FDQUY7O0FER0E7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQ0FGOztBRENFO0VBQ0UsY0FBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWljb24ge1xyXG4gIG1hcmdpbjogNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1mb250LCAjZmZmZmZmKTtcclxuICAvLyBib3gtc2hhZG93OiAwcHggMnB4IDBweCB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZikgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQsICMwMDAwMDApO1xyXG59XHJcblxyXG5pb24taXRlbSB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICBib3JkZXI6IG5vbmU7fVxyXG4vLyBpb24tYnV0dG9uIHtcclxuLy8gICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xyXG4vLyB9XHJcbmlvbi1ncmlkIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWZvbnQsICNmZmZmZmYpO1xyXG4gIG1hcmdpbjogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOENCNThEO1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbmlvbi1pbnB1dCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcclxufVxyXG4udGVybXNfd3JhcHBlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTFweDtcclxuICBtYXJnaW46IDFlbSAwO1xyXG4gIGEge1xyXG4gICAgY29sb3I6ICMwMDA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gIH1cclxufVxyXG5cclxuLnRlcm1zX2NoZWNrYm94IHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uYWxyZWFkeV9oYXZlX2FjY291bnQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAyNXB4O1xyXG59XHJcblxyXG4uc2lnbi11cC1idXR0b24tcm93IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAxZW0gMDtcclxufVxyXG5cclxuLnNpZ24tdXAtYnV0dG9uIHtcclxuICAtLWJvcmRlci1yYWRpdXM6IDA7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNXZ3O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDE1dnc7XHJcbn1cclxuXHJcbi5pb24tdG9vbGJhci1jdXN0b20ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgcGFkZGluZy1yaWdodDogMTBweDtcclxufVxyXG5cclxuLmhhdmVfYWNjb3VudF9saW5rIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5mb3JnZXRfcGFzc19zdGF0ZW1lbnQge1xyXG4gIHBhZGRpbmctbGVmdDogMTVweDtcclxufVxyXG5cclxuLmJhY2staWNvbiB7XHJcbiAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcblxyXG4uc2lnbi1sb2dpbi1yb3cge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxuICBzcGFuIHtcclxuICAgIG1hcmdpbjogMTBweCAwO1xyXG4gIH1cclxufSIsImlvbi1pY29uIHtcbiAgbWFyZ2luOiA1cHggIWltcG9ydGFudDtcbn1cblxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBlIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZCwgIzAwMDAwMCk7XG59XG5cbmlvbi1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgYm9yZGVyOiBub25lO1xufVxuXG5pb24tZ3JpZCB7XG4gIGNvbG9yOiB2YXIoLS1pb24tZm9udCwgI2ZmZmZmZik7XG4gIG1hcmdpbjogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzhDQjU4RDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbn1cblxuaW9uLWlucHV0IHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcbn1cblxuLnRlcm1zX3dyYXBwZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbjogMWVtIDA7XG59XG4udGVybXNfd3JhcHBlciBhIHtcbiAgY29sb3I6ICMwMDA7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cblxuLnRlcm1zX2NoZWNrYm94IHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLmFscmVhZHlfaGF2ZV9hY2NvdW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyNXB4O1xufVxuXG4uc2lnbi11cC1idXR0b24tcm93IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDFlbSAwO1xufVxuXG4uc2lnbi11cC1idXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDA7XG4gIC0tcGFkZGluZy1zdGFydDogMTV2dztcbiAgLS1wYWRkaW5nLWVuZDogMTV2dztcbn1cblxuLmlvbi10b29sYmFyLWN1c3RvbSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG5cbi5oYXZlX2FjY291bnRfbGluayB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmZvcmdldF9wYXNzX3N0YXRlbWVudCB7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbn1cblxuLmJhY2staWNvbiB7XG4gIGZvbnQtc2l6ZTogMjRweDtcbn1cblxuLnNpZ24tbG9naW4tcm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbn1cbi5zaWduLWxvZ2luLXJvdyBzcGFuIHtcbiAgbWFyZ2luOiAxMHB4IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _config_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../config/constants */ "./src/app/config/constants.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../services/storage.service */ "./src/app/services/storage.service.ts");
/* harmony import */ var _services_toast_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../services/toast.service */ "./src/app/services/toast.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/webview.service */ "./src/app/services/webview.service.ts");










let LoginPage = class LoginPage {
    constructor(router, authService, storageService, toastService, _translate, languageService, webviewService) {
        this.router = router;
        this.authService = authService;
        this.storageService = storageService;
        this.toastService = toastService;
        this._translate = _translate;
        this.languageService = languageService;
        this.webviewService = webviewService;
        this.postData = {
            profile_id: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].PROFILEID,
            api_key: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].APIKEY,
            usermail: "",
            password: "",
            app_version: "",
            request_operation: "user_login",
            language: _config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].LANGUAGE,
        };
        this.loading = false;
    }
    ngOnInit() {
        this.userLanguage = this.languageService.getUserLanguage();
    }
    goToWebview() {
        this.webviewService.goToWebview();
    }
    goToSignUp() {
        this.router.navigate(["/signup"]);
    }
    goToForgotPassword() {
        this.router.navigate(["/forgot-password"]);
    }
    goToPrivacyLink() {
        window.open(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].PRIVACYLINK);
    }
    goToTermsLink() {
        window.open(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].TERMSLINK);
    }
    validateInputs() {
        console.log(this.postData);
        let usermail = this.postData.usermail.trim();
        let password = this.postData.password.trim();
        return (this.postData.usermail &&
            this.postData.password &&
            usermail.length > 0 &&
            password.length > 0);
    }
    loginAction() {
        if (this.validateInputs()) {
            this.authService.login(this.postData).subscribe((res) => {
                console.log(res);
                if (res.success == "true") {
                    //Storing the User data.
                    const userData = Object.assign({}, res.response);
                    userData["session_id"] = res.session_id;
                    userData["session_name"] = res.session_name;
                    this.storageService.store(_config_constants__WEBPACK_IMPORTED_MODULE_3__["Constants"].AUTH, userData).then((res) => {
                        this.router.navigate(["home"]);
                    });
                }
                else {
                    this.toastService.presentToast(this._translate.instant("Response_Message", {
                        value: res.error.message,
                    }));
                }
            }, (err) => {
                console.log(err);
                this.toastService.presentToast(this._translate.instant("Error_Message"));
            });
        }
        else {
            this.toastService.presentToast(this._translate.instant("Invalid_Credentials"));
        }
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] },
    { type: _services_toast_service__WEBPACK_IMPORTED_MODULE_6__["ToastService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"] },
    { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_8__["LanguageService"] },
    { type: src_app_services_webview_service__WEBPACK_IMPORTED_MODULE_9__["WebviewService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-login",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")).default]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map