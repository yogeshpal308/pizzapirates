function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["web"], {
  /***/
  "./node_modules/@capacitor/app/dist/esm/web.js":
  /*!*****************************************************!*\
    !*** ./node_modules/@capacitor/app/dist/esm/web.js ***!
    \*****************************************************/

  /*! exports provided: AppWeb */

  /***/
  function node_modulesCapacitorAppDistEsmWebJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppWeb", function () {
      return AppWeb;
    });
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/index.js");

    var AppWeb = /*#__PURE__*/function (_capacitor_core__WEBP) {
      _inherits(AppWeb, _capacitor_core__WEBP);

      var _super = _createSuper(AppWeb);

      function AppWeb() {
        var _this;

        _classCallCheck(this, AppWeb);

        _this = _super.call(this);

        _this.handleVisibilityChange = function () {
          var data = {
            isActive: document.hidden !== true
          };

          _this.notifyListeners('appStateChange', data);

          if (document.hidden) {
            _this.notifyListeners('pause', null);
          } else {
            _this.notifyListeners('resume', null);
          }
        };

        document.addEventListener('visibilitychange', _this.handleVisibilityChange, false);
        return _this;
      }

      _createClass(AppWeb, [{
        key: "exitApp",
        value: function exitApp() {
          throw this.unimplemented('Not implemented on web.');
        }
      }, {
        key: "getInfo",
        value: function () {
          var _getInfo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    throw this.unimplemented('Not implemented on web.');

                  case 1:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          function getInfo() {
            return _getInfo.apply(this, arguments);
          }

          return getInfo;
        }()
      }, {
        key: "getLaunchUrl",
        value: function () {
          var _getLaunchUrl = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    return _context2.abrupt("return", {
                      url: ''
                    });

                  case 1:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2);
          }));

          function getLaunchUrl() {
            return _getLaunchUrl.apply(this, arguments);
          }

          return getLaunchUrl;
        }()
      }, {
        key: "getState",
        value: function () {
          var _getState = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    return _context3.abrupt("return", {
                      isActive: document.hidden !== true
                    });

                  case 1:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3);
          }));

          function getState() {
            return _getState.apply(this, arguments);
          }

          return getState;
        }()
      }, {
        key: "minimizeApp",
        value: function () {
          var _minimizeApp = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    throw this.unimplemented('Not implemented on web.');

                  case 1:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));

          function minimizeApp() {
            return _minimizeApp.apply(this, arguments);
          }

          return minimizeApp;
        }()
      }]);

      return AppWeb;
    }(_capacitor_core__WEBPACK_IMPORTED_MODULE_0__["WebPlugin"]); //# sourceMappingURL=web.js.map

    /***/

  },

  /***/
  "./node_modules/@capacitor/device/dist/esm/web.js":
  /*!********************************************************!*\
    !*** ./node_modules/@capacitor/device/dist/esm/web.js ***!
    \********************************************************/

  /*! exports provided: DeviceWeb */

  /***/
  function node_modulesCapacitorDeviceDistEsmWebJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeviceWeb", function () {
      return DeviceWeb;
    });
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/index.js");

    var DeviceWeb = /*#__PURE__*/function (_capacitor_core__WEBP2) {
      _inherits(DeviceWeb, _capacitor_core__WEBP2);

      var _super2 = _createSuper(DeviceWeb);

      function DeviceWeb() {
        _classCallCheck(this, DeviceWeb);

        return _super2.apply(this, arguments);
      }

      _createClass(DeviceWeb, [{
        key: "getId",
        value: function () {
          var _getId = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    return _context5.abrupt("return", {
                      uuid: this.getUid()
                    });

                  case 1:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));

          function getId() {
            return _getId.apply(this, arguments);
          }

          return getId;
        }()
      }, {
        key: "getInfo",
        value: function () {
          var _getInfo2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var ua, uaFields;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    if (!(typeof navigator === 'undefined' || !navigator.userAgent)) {
                      _context6.next = 2;
                      break;
                    }

                    throw this.unavailable('Device API not available in this browser');

                  case 2:
                    ua = navigator.userAgent;
                    uaFields = this.parseUa(ua);
                    return _context6.abrupt("return", {
                      model: uaFields.model,
                      platform: 'web',
                      operatingSystem: uaFields.operatingSystem,
                      osVersion: uaFields.osVersion,
                      manufacturer: navigator.vendor,
                      isVirtual: false,
                      webViewVersion: uaFields.browserVersion
                    });

                  case 5:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));

          function getInfo() {
            return _getInfo2.apply(this, arguments);
          }

          return getInfo;
        }()
      }, {
        key: "getBatteryInfo",
        value: function () {
          var _getBatteryInfo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var battery;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    if (!(typeof navigator === 'undefined' || !navigator.getBattery)) {
                      _context7.next = 2;
                      break;
                    }

                    throw this.unavailable('Device API not available in this browser');

                  case 2:
                    battery = {};
                    _context7.prev = 3;
                    _context7.next = 6;
                    return navigator.getBattery();

                  case 6:
                    battery = _context7.sent;
                    _context7.next = 11;
                    break;

                  case 9:
                    _context7.prev = 9;
                    _context7.t0 = _context7["catch"](3);

                  case 11:
                    return _context7.abrupt("return", {
                      batteryLevel: battery.level,
                      isCharging: battery.charging
                    });

                  case 12:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this, [[3, 9]]);
          }));

          function getBatteryInfo() {
            return _getBatteryInfo.apply(this, arguments);
          }

          return getBatteryInfo;
        }()
      }, {
        key: "getLanguageCode",
        value: function () {
          var _getLanguageCode = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    return _context8.abrupt("return", {
                      value: navigator.language.split('-')[0].toLowerCase()
                    });

                  case 1:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8);
          }));

          function getLanguageCode() {
            return _getLanguageCode.apply(this, arguments);
          }

          return getLanguageCode;
        }()
      }, {
        key: "getLanguageTag",
        value: function () {
          var _getLanguageTag = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    return _context9.abrupt("return", {
                      value: navigator.language
                    });

                  case 1:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9);
          }));

          function getLanguageTag() {
            return _getLanguageTag.apply(this, arguments);
          }

          return getLanguageTag;
        }()
      }, {
        key: "parseUa",
        value: function parseUa(ua) {
          var uaFields = {};
          var start = ua.indexOf('(') + 1;
          var end = ua.indexOf(') AppleWebKit');

          if (ua.indexOf(') Gecko') !== -1) {
            end = ua.indexOf(') Gecko');
          }

          var fields = ua.substring(start, end);

          if (ua.indexOf('Android') !== -1) {
            var tmpFields = fields.replace('; wv', '').split('; ').pop();

            if (tmpFields) {
              uaFields.model = tmpFields.split(' Build')[0];
            }

            uaFields.osVersion = fields.split('; ')[1];
          } else {
            uaFields.model = fields.split('; ')[0];

            if (typeof navigator !== 'undefined' && navigator.oscpu) {
              uaFields.osVersion = navigator.oscpu;
            } else {
              if (ua.indexOf('Windows') !== -1) {
                uaFields.osVersion = fields;
              } else {
                var _tmpFields = fields.split('; ').pop();

                if (_tmpFields) {
                  var lastParts = _tmpFields.replace(' like Mac OS X', '').split(' ');

                  uaFields.osVersion = lastParts[lastParts.length - 1].replace(/_/g, '.');
                }
              }
            }
          }

          if (/android/i.test(ua)) {
            uaFields.operatingSystem = 'android';
          } else if (/iPad|iPhone|iPod/.test(ua) && !window.MSStream) {
            uaFields.operatingSystem = 'ios';
          } else if (/Win/.test(ua)) {
            uaFields.operatingSystem = 'windows';
          } else if (/Mac/i.test(ua)) {
            uaFields.operatingSystem = 'mac';
          } else {
            uaFields.operatingSystem = 'unknown';
          } // Check for browsers based on non-standard javascript apis, only not user agent


          var isFirefox = !!window.InstallTrigger;
          var isSafari = !!window.ApplePaySession;
          var isChrome = !!window.chrome;
          var isEdge = /Edg/.test(ua);
          var isFirefoxIOS = /FxiOS/.test(ua);
          var isChromeIOS = /CriOS/.test(ua);
          var isEdgeIOS = /EdgiOS/.test(ua); // FF and Edge User Agents both end with "/MAJOR.MINOR"

          if (isSafari || isChrome && !isEdge || isFirefoxIOS || isChromeIOS || isEdgeIOS) {
            // Safari version comes as     "... Version/MAJOR.MINOR ..."
            // Chrome version comes as     "... Chrome/MAJOR.MINOR ..."
            // FirefoxIOS version comes as "... FxiOS/MAJOR.MINOR ..."
            // ChromeIOS version comes as  "... CriOS/MAJOR.MINOR ..."
            var searchWord;

            if (isFirefoxIOS) {
              searchWord = 'FxiOS';
            } else if (isChromeIOS) {
              searchWord = 'CriOS';
            } else if (isEdgeIOS) {
              searchWord = 'EdgiOS';
            } else if (isSafari) {
              searchWord = 'Version';
            } else {
              searchWord = 'Chrome';
            }

            var words = ua.split(' ');

            var _iterator = _createForOfIteratorHelper(words),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var word = _step.value;

                if (word.includes(searchWord)) {
                  var version = word.split('/')[1];
                  uaFields.browserVersion = version;
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          } else if (isFirefox || isEdge) {
            var reverseUA = ua.split('').reverse().join('');
            var reverseVersion = reverseUA.split('/')[0];

            var _version = reverseVersion.split('').reverse().join('');

            uaFields.browserVersion = _version;
          }

          return uaFields;
        }
      }, {
        key: "getUid",
        value: function getUid() {
          if (typeof window !== 'undefined' && window.localStorage) {
            var uid = window.localStorage.getItem('_capuid');

            if (uid) {
              return uid;
            }

            uid = this.uuid4();
            window.localStorage.setItem('_capuid', uid);
            return uid;
          }

          return this.uuid4();
        }
      }, {
        key: "uuid4",
        value: function uuid4() {
          return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0,
                v = c === 'x' ? r : r & 0x3 | 0x8;
            return v.toString(16);
          });
        }
      }]);

      return DeviceWeb;
    }(_capacitor_core__WEBPACK_IMPORTED_MODULE_0__["WebPlugin"]); //# sourceMappingURL=web.js.map

    /***/

  }
}]);
//# sourceMappingURL=web-es5.js.map